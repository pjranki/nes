#pragma once

#include "cpu.h"
#include "nes_emu.h"  // this whole file exists because of forward declaring nes::emu::CTX

namespace nes
{
namespace status
{
    template <class T, int bits>
    static inline void setZ(emu::CTX *emu, const bit_field<T, bits>& result)
    {
        emu->cpu.P.change<F_ZERO>(result.zero());
    }

    template <class T, int bits>
    static inline void setN(emu::CTX *emu, const bit_field<T, bits>& result)
    {
        emu->cpu.P.change<F_NEGATIVE>(result.negative());
    }

    template <class T, int bits>
    static inline void setNZ(emu::CTX *emu, const bit_field<T, bits>& result)
    {
        emu->cpu.P.change<F_NEGATIVE>(result.negative());
        emu->cpu.P.change<F_ZERO>(result.zero());
    }

    template <class T, int bits>
    static inline void setNV(emu::CTX *emu, const bit_field<T, bits>& result)
    {
        STATIC_ASSERT((int)F_NEGATIVE == 1 << 7 && (int)F_OVERFLOW == 1 << 6);
        emu->cpu.P.copy<F__NV, 6, 2>(result);
    }
}

namespace bitshift
{
    template <class T, int bits>
    static inline void ASL(emu::CTX *emu, bit_field<T, bits>& operand) {
        // Arithmetic Shift Left
        emu->cpu.P.change<F_CARRY>(MSB(operand));
        operand.selfShl1();
        status::setNZ(emu, operand);
    }

    template <class T, int bits>
    static inline void LSR(emu::CTX *emu, bit_field<T, bits>& operand) {
        // Logical Shift Right
        emu->cpu.P.change<F_CARRY>(LSB(operand));
        operand.selfShr1();
        status::setZ(emu, operand);
        emu->cpu.P.clear(F_SIGN);
    }

    template <class T, int bits>
    static inline void ROL(emu::CTX *emu, bit_field<T, bits>& operand) {
        // Rotate Left With Carry
        const bool newCarry = MSB(operand);
        operand.selfRcl((emu->cpu.P)[F_CARRY]);
        emu->cpu.P.change<F_CARRY>(newCarry);
        status::setNZ(emu, operand);
    }

    template <class T, int bits>
    static inline void ROR(emu::CTX *emu, bit_field<T, bits>& operand) {
        // Rotate Right With Carry
        const bool newCarry = LSB(operand);
        operand.selfRcr((emu->cpu.P)[F_CARRY]);
        emu->cpu.P.change<F_CARRY>(newCarry);
        status::setNZ(emu, operand);
    }
}
}
