#include "macros.h"
#include "types.h"
#include "internals.h"
#include "debug.h"
#include "rom.h"
#include "opcodes.h"
#include "mmc.h"
#include "cpu.h"
#include "ppu.h"
#include "nes_emu.h"

namespace nes
{
namespace emu
{
	void init(emu::CTX *emu)
	{
		opcode::initTable(emu);
		ppu::init(emu);
	}

	void deinit(emu::CTX *emu)
	{
		rom::unload(emu);
	}

	bool load(emu::CTX *emu, uint8_t *rom_image, uint32_t rom_size)
	{
		return rom::load(emu, rom_image, rom_size);
	}

	void reset(emu::CTX *emu)
	{
		// reset mmc
		mmc::reset(emu);
		mapper::reset(emu);

		// reset cpu
		cpu::reset(emu);

		// reset ppu
		ppu::reset(emu);
	}

	bool setup(emu::CTX *emu)
	{
		if (!mapper::setup(emu)) return false;
		if (!pmapper::setup(emu)) return false;
		return true;
	}

	bool nextFrame(emu::CTX *emu)
	{
		for (;;)
		{
			if (cpu::run(emu, -1, SCANLINE_CYCLES))
			{
				if (!ppu::hsync(emu))
				{
					// frame ends
					break;
				}
			}
            else
            {
                return false; // program stops
            }
		}
		return true;
	}

	bool tick(emu::CTX *emu) {
		emu->ui.doEvents(emu->ui.ctx);
		if (emu->ui.forceTerminate(emu->ui.ctx))
		{
			// create state dump on force termination
			cpu::dump(emu);
			return false;
		}
		if (!nextFrame(emu))
		{
			// game stops
			return false;
		}
		emu->ui.limitFPS(emu->ui.ctx);
		return true;
	}

	void run(emu::CTX *emu)
	{
		for (;;)
		{
            if (!tick(emu))
			{
				break;
			}
		}
	}

	long long frameCount(emu::CTX *emu)
	{
		return ppu::currentFrame(emu);
	}

	void present(emu::CTX *emu, const int y, const palindex_t line[], const int line_width, const rgb16_t *palette)
	{
        emu->ui.renderLine(emu->ui.ctx, y, line, line_width, palette);
	}

	void onFrameBegin(emu::CTX *emu)
	{
        emu->ui.onFrameBegin(emu->ui.ctx);
	}

	void onFrameEnd(emu::CTX *emu)
	{
        emu->ui.onFrameEnd(emu->ui.ctx);
	}

	void saveState(emu::CTX *emu, FILE *fp)
	{
		mmc::save(emu, fp);
		cpu::save(emu, fp);
		ppu::save(emu, fp);
		mapper::save(emu, fp);
	}

	void loadState(emu::CTX *emu, FILE *fp)
	{
		reset(emu); // necessary

		mmc::load(emu, fp);
		cpu::load(emu, fp);
		ppu::load(emu, fp);
		mapper::load(emu, fp);
	}
}
}
