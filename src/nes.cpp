#include "nes.h"

namespace nes {

    void NESAPI Emulator::renderLine(void *ctx, const int y, const nes::palindex_t line[], const int line_size, const nes::rgb16_t *palette)
    {
        nes::Emulator::RenderLineFn &func = reinterpret_cast<nes::Emulator*>(ctx)->render_line_;
        if (nullptr != func) {
            func((uint16_t)y, line, (uint16_t)line_size, palette);
        }
    }

    bool NESAPI Emulator::hasInput(void *ctx, const int player)
    {
        return true;
    }

    void NESAPI Emulator::resetInput(void *ctx)
    {
        
    }

    int NESAPI Emulator::readInput(void *ctx, const int player)
    {
        return 0;
    }

    bool NESAPI Emulator::forceTerminate(void *ctx)
    {
        return false;
    }

    void NESAPI Emulator::limitFPS(void *ctx)
    {
    
    }

    void NESAPI Emulator::onFrameBegin(void *ctx)
    {
        nes::Emulator::FrameStartFn &func = reinterpret_cast<nes::Emulator*>(ctx)->frame_start_;
        if (nullptr != func) {
            func();
        }
    }

    void NESAPI Emulator::onFrameEnd(void *ctx)
    {
        nes::Emulator::FrameEndFn &func = reinterpret_cast<nes::Emulator*>(ctx)->frame_end_;
        if (nullptr != func) {
            func();
        }
    }

    void NESAPI Emulator::doEvents(void *ctx)
    {
    
    }

    void NESAPI Emulator::customPrint(void *ctx, const char *message)
    {
        nes::Emulator::LogFn &func = reinterpret_cast<nes::Emulator*>(ctx)->log_;
        if (nullptr != func) {
            func(message);
        }
    }

    void NESAPI Emulator::customMemcpy(void *ctx, void *dst, void *src, uint32_t size)
    {
        nes::Emulator::MemcpyFn &func = reinterpret_cast<nes::Emulator*>(ctx)->memcpy_;
        if (nullptr != func) {
            func(dst, src, size);
        }
    }

}
