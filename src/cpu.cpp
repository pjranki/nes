#include "macros.h"
#include "types.h"
#include "internals.h"
#include "debug.h"
#include "mmc.h"
#include "mmc_template.h"
#include "opcodes.h"
#include "cpu.h"
#include "cpu_template.h"
#include "nes_emu.h"
#include <string.h>

namespace nes
{
namespace stack
{
	inline void pushByte(emu::CTX *emu, const byte_t byte)
	{
#ifdef MONITOR_STACK
		debug::info(emu, "[S] Push 0x%02X to $%02X\n",byte,valueOf(SP));
#endif

		ramSt[emu->cpu.SP]=byte;

		dec(emu->cpu.SP);
#ifndef ALLOW_ADDRESS_WRAP
		ERROR_IF(emu->cpu.SP.reachMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);
#endif
	}

	inline void pushReg(emu::CTX *emu, const reg_bit_field_t& reg)
	{
		pushByte(emu, reg);
	}

	inline void pushWord(emu::CTX *emu, const word_t word)
	{
		dec(emu->cpu.SP);
#ifdef MONITOR_STACK
		debug::info(emu, "[S] Push 0x%04X to $%02X\n",word,valueOf(SP));
#endif
		FATAL_ERROR_IF(emu->cpu.SP.reachMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);

		setWord(&(ramSt[emu->cpu.SP]), word);

		dec(emu->cpu.SP);
#ifndef ALLOW_ADDRESS_WRAP
		ERROR_IF(emu->cpu.SP.reachMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);
#endif
	}

	inline void pushPC(emu::CTX *emu)
	{
		pushWord(emu, emu->cpu.PC);
	}

	inline byte_t popByte(emu::CTX *emu)
	{
#ifndef ALLOW_ADDRESS_WRAP
		ERROR_IF(emu->cpu.SP.reachMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);
#endif
		return ramSt[inc(emu->cpu.SP)];
	}

	inline word_t popWord(emu::CTX *emu)
	{
#ifndef ALLOW_ADDRESS_WRAP
		ERROR_IF(emu->cpu.SP.plus(1).reachMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);
#endif
		FATAL_ERROR_UNLESS(emu->cpu.SP.belowMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);

		(emu->cpu.SP)+=2;
		return getWord(&(ramSt[emu->cpu.SP.minus(1)]));
	}

	void reset(emu::CTX *emu)
	{
		// move stack pointer to the top of the stack
		emu->cpu.SP.selfSetMax();

		// clear stack
		memset(ramSt, 0, sizeof(ramSt));
	}
}

namespace interrupt
{
	static const maddr_t VECTOR_NMI(0xFFFA);
	static const maddr_t VECTOR_RESET(0xFFFC);
	static const maddr_t VECTOR_IRQ(0xFFFE);

	static void clearAll(emu::CTX *emu)
	{
		emu->cpu.pendingIRQs.clearAll();
	}

	static void clear(emu::CTX *emu, IRQTYPE type)
	{
		assert(emu->cpu.pendingIRQs[type]);
        emu->cpu.pendingIRQs.clear(type);
	}

	void request(emu::CTX *emu, const IRQTYPE type)
	{
		vassert(type != IRQTYPE::NONE);
		ERROR_IF(emu->cpu.pendingIRQs[type], ILLEGAL_OPERATION, IRQ_ALREADY_PENDING);

        emu->cpu.pendingIRQs.set(type);
		STAT_ADD(emu->cpu.totInterrupts, 1);
	}

	// get address of interrupt handler
	static maddr_t handler(emu::CTX *emu, const IRQTYPE type)
	{
		vassert(type != IRQTYPE::NONE);

		maddr_t vector;
		switch (type)
		{
		case IRQTYPE::RST: vector=VECTOR_RESET; break;
		case IRQTYPE::NMI: vector=VECTOR_NMI; break;

		case IRQTYPE::IRQ:
		case IRQTYPE::BRK:
			vector=VECTOR_IRQ;
			break;
		}
		return mmc::fetchWordOperand(emu, vector);
	}

	static bool pending(emu::CTX *emu)
	{
		return emu->cpu.pendingIRQs.any();
	}

	static bool pending(emu::CTX *emu, const IRQTYPE type)
	{
		vassert(type != IRQTYPE::NONE);
		return emu->cpu.pendingIRQs[type];
	}

	// return the highest-priority IRQ that is currently pending
	static IRQTYPE current(emu::CTX *emu)
	{
		if (emu->cpu.pendingIRQs[IRQTYPE::RST]) return IRQTYPE::RST;
		if (emu->cpu.pendingIRQs[IRQTYPE::NMI]) return IRQTYPE::NMI;
		if (emu->cpu.pendingIRQs[IRQTYPE::IRQ]) return IRQTYPE::IRQ;
		if (emu->cpu.pendingIRQs[IRQTYPE::BRK]) return IRQTYPE::BRK;
		return IRQTYPE::NONE;
	}

	static void poll(emu::CTX *emu)
	{
		if (pending(emu))
		{
			IRQTYPE irq = current(emu);
			if (irq != IRQTYPE::IRQ || !(emu->cpu.P)[F_INTERRUPT_OFF])
			{
				// process IRQ
				stack::pushPC(emu);
				if (irq != IRQTYPE::RST)
				{
					// set or clear Break flag depending on irq type
					if (irq == IRQTYPE::BRK)
						(emu->cpu.P)|=F_BREAK;
					else
						(emu->cpu.P)-=F_BREAK;
					// push status
					stack::pushReg(emu, emu->cpu.P);
					// disable other interrupts
					(emu->cpu.P)|=F_INTERRUPT_OFF;
				}
				// jump to interrupt handler
				emu->cpu.PC = handler(emu, irq);
				clear(emu, irq);
			}
		}
	}
}

namespace arithmetic
{
	static void ADC(emu::CTX *emu)
	{
		// Add with carry. A <- [A]+[M]+C
#ifdef WANT_BCD
		if ((emu->cpu.P)[F_BCD])
		{
			// bcd addition
			assert(((emu->cpu.A)&0xF)<=9 && ((emu->cpu.A)>>4)<=9);
			assert(((emu->cpu.value)&0xF)<=9 && ((emu->cpu.value)>>4)<=9);
			// add CF
			(emu->cpu.temp)=(emu->cpu.A)+((emu->cpu.P)[F_CARRY]?1:0);

			// add low digit
			if (((emu->cpu.temp)&0xF)+((emu->cpu.value)&0xF)>9)
			{
				(emu->cpu.temp)+=((emu->cpu.value)&0xF)+6;
				(emu->cpu.P)|=F_CARRY;
			}else
			{
				(emu->cpu.temp)+=((emu->cpu.value)&0xF);
				(emu->cpu.P)-=F_CARRY;
			}
			if (((emu->cpu.temp)>>4)+((emu->cpu.value)>>4)>9)
			{
				(emu->cpu.temp)+=((emu->cpu.value)&0xF0)+6;
				(emu->cpu.P)|=F_OVERFLOW;
				(emu->cpu.P)|=F_CARRY;
			}else
			{
				(emu->cpu.temp)+=((emu->cpu.value)&0xF0);
				(emu->cpu.P)-=F_OVERFLOW;
			}
		}else
#endif
		{
			// binary addition
			(emu->cpu.temp)=(emu->cpu.A)+(emu->cpu.value)+((emu->cpu.P)[F_CARRY]?1:0);
			emu->cpu.P.change<F_OVERFLOW>(!(((emu->cpu.A)^(emu->cpu.value))&0x80) && (((emu->cpu.A)^(emu->cpu.temp))&0x80));
			(emu->cpu.P).change<F_CARRY>(SUM.overflow());
		}
		status::setNZ(emu, regA(emu->cpu.temp));
	}

	static void SBC(emu::CTX *emu)
	{
#ifdef WANT_BCD_MODE
		// SBC is currently impossible in decimal mode
		assert(!(emu->cpu.P)[F_BCD]);
#endif
		(emu->cpu.temp)=(emu->cpu.A)-(emu->cpu.value)-((emu->cpu.P)[F_CARRY]?0:1);

		emu->cpu.P.change<F_CARRY>(!SUM.overflow());
		emu->cpu.P.change<F_OVERFLOW>((((emu->cpu.A)^(emu->cpu.value))&0x80) && (((emu->cpu.A)^(emu->cpu.temp))&0x80));

		status::setNZ(emu, regA(emu->cpu.temp));
	}
}

namespace cpu
{
	void reset(emu::CTX *emu)
	{
		// reset general purpose registers
		(emu->cpu.A)=0;
		(emu->cpu.X)=0;
		(emu->cpu.Y)=0;
		(emu->cpu.PC)=0;

		// reset status register
		(emu->cpu.P).clearAll();
		(emu->cpu.P).set(F_RESERVED);
		(emu->cpu.P).set(F_INTERRUPT_OFF);

		// reset stack pointer
		stack::reset(emu);

		// reset IRQ state
		interrupt::clearAll(emu);
		// this will set PC to the entry point
		interrupt::request(emu, IRQTYPE::RST);

		emu->cpu.remainingCycles = 0;
		// others
#ifdef WANT_RUN_HIT
		for (int i=0;i<0x8000;i++)
			(emu->cpu.instructionHit)[i]=false;
#endif
	}

	void save(emu::CTX *emu, FILE *fp)
	{
		// registers
		fwrite(&(emu->cpu.A), sizeof(emu->cpu.A), 1, fp);
		fwrite(&(emu->cpu.X), sizeof(emu->cpu.X), 1, fp);
		fwrite(&(emu->cpu.Y), sizeof(emu->cpu.Y), 1, fp);
		fwrite(&(emu->cpu.SP), sizeof(emu->cpu.SP), 1, fp);
		fwrite(&(emu->cpu.P), sizeof(emu->cpu.P), 1, fp);
		fwrite(&(emu->cpu.PC), sizeof(emu->cpu.PC), 1, fp);
		fwrite(&(emu->cpu.pendingIRQs), sizeof(emu->cpu.pendingIRQs), 1, fp);
	}
	
	void load(emu::CTX *emu, FILE *fp)
	{
		// registers
		fread(&(emu->cpu.A), sizeof(emu->cpu.A), 1, fp);
		fread(&(emu->cpu.X), sizeof(emu->cpu.X), 1, fp);
		fread(&(emu->cpu.Y), sizeof(emu->cpu.Y), 1, fp);
		fread(&(emu->cpu.SP), sizeof(emu->cpu.SP), 1, fp);
		fread(&(emu->cpu.P), sizeof(emu->cpu.P), 1, fp);
		fread(&(emu->cpu.PC), sizeof(emu->cpu.PC), 1, fp);
		fread(&(emu->cpu.pendingIRQs), sizeof(emu->cpu.pendingIRQs), 1, fp);
	}

	void dump(emu::CTX *emu)
    {
#ifdef WANT_RUN_HIT
		FILE *fp=fopen("RUNHIT.log","wt");
		for (int i=0;i<0x8000;i++)
		{
			if ((emu->cpu.instructionHit)[i])
			{
				fprintf(fp, "%X\n", i|0x8000);
			}
		}
		fclose(fp);
#endif
	}

	// emulate at most n instructions within specified cycles
	bool run(emu::CTX *emu, int n, long cycles)
	{
		(emu->cpu.remainingCycles)+=cycles;
		while ((n<0 || n--) && (emu->cpu.remainingCycles)>0)
		{
			int cyc;
			cyc=nextInstruction(emu);
			if (cyc<0) return false; // execution terminated
		}
		return true;
	}

	void irq(emu::CTX *emu, const IRQTYPE type)
	{
		interrupt::request(emu, type);
	}

	static int readEffectiveAddress(emu::CTX *emu, const opcode_t code, const opcode::M6502_OPCODE op, bool forWriteOnly = false)
	{
		int cycles=0;
		
		invalidate(EA);
		invalidate(M);

		maddr8_t addr8;

		switch (op.addrmode)
		{
        case opcode::ADR_IMP: // Ignore. Address is implied in instruction.
			break;

		case opcode::ADR_ZP: // Zero Page mode. Use the address given after the opcode, but without high byte.
			addr8=mmc::fetchByteOperand(emu, emu->cpu.PC);
			emu->cpu.addr=addr8;
			emu->cpu.value=mmc::loadZPByte(emu, addr8);
			break;

		case opcode::ADR_REL: // Relative mode.
			emu->cpu.addr=valueOf(mmc::fetchByteOperand(emu, emu->cpu.PC));
			if ((emu->cpu.addr)[7])
			{
				// sign extension
				(emu->cpu.addr)|=maddr_t(0xFF00);
			}
			(emu->cpu.addr)+=(emu->cpu.PC);
			break;

		case opcode::ADR_ABS: // Absolute mode. Use the two bytes following the opcode as an address.
			(emu->cpu.addr)=mmc::fetchWordOperand(emu, emu->cpu.PC);
			if (!forWriteOnly) (emu->cpu.value)=mmc::read(emu, emu->cpu.addr);
			break;

		case opcode::ADR_IMM: //Immediate mode. The value is given after the opcode.
			emu->cpu.addr=emu->cpu.PC;
			emu->cpu.value=mmc::fetchByteOperand(emu, emu->cpu.PC);
			break;

		case opcode::ADR_ZPX:
			// Zero Page Indexed mode, X as index. Use the address given
			// after the opcode, then add the
			// X register to it to get the final address.
			addr8=mmc::fetchByteOperand(emu, emu->cpu.PC).plus(emu->cpu.X);
			emu->cpu.addr=addr8;
			emu->cpu.value=mmc::loadZPByte(emu, addr8);
			break;

		case opcode::ADR_ZPY:
			// Zero Page Indexed mode, Y as index. Use the address given
			// after the opcode, then add the
			// Y register to it to get the final address.
			addr8=mmc::fetchByteOperand(emu, emu->cpu.PC).plus(emu->cpu.Y);
			emu->cpu.addr=addr8;
			emu->cpu.value=mmc::loadZPByte(emu, addr8);
			break;

		case opcode::ADR_ABSX:
			// Absolute Indexed Mode, X as index. Same as zero page
			// indexed, but with the high byte.
			emu->cpu.addr=mmc::fetchWordOperand(emu, emu->cpu.PC);
			if ((valueOf(emu->cpu.addr)&0xFF00)!=((valueOf(emu->cpu.addr)+(emu->cpu.X))&0xFF00)) ++cycles;
			(emu->cpu.addr)+=(emu->cpu.X);
			if (!forWriteOnly) (emu->cpu.value)=mmc::read(emu, emu->cpu.addr);
			break;

		case opcode::ADR_ABSY:
			// Absolute Indexed Mode, Y as index. Same as zero page
			// indexed, but with the high byte.
			emu->cpu.addr=mmc::fetchWordOperand(emu, emu->cpu.PC);
			if ((valueOf(emu->cpu.addr)&0xFF00)!=((valueOf(emu->cpu.addr)+(emu->cpu.Y))&0xFF00)) ++cycles;
			(emu->cpu.addr)+=(emu->cpu.Y);
			if (!forWriteOnly) (emu->cpu.value)=mmc::read(emu, emu->cpu.addr);
			break;

		case opcode::ADR_INDX:
			addr8=mmc::fetchByteOperand(emu, emu->cpu.PC).plus(emu->cpu.X);
			emu->cpu.addr=mmc::loadZPWord(emu, addr8);
			if (!forWriteOnly) (emu->cpu.value)=mmc::read(emu, emu->cpu.addr);
			break;

		case opcode::ADR_INDY:
			(emu->cpu.addr)=mmc::loadZPWord(emu, mmc::fetchByteOperand(emu, (emu->cpu.PC)));
			if ((valueOf(emu->cpu.addr)&0xFF00)!=((valueOf(emu->cpu.addr)+(emu->cpu.Y))&0xFF00)) ++cycles;
			(emu->cpu.addr)+=(emu->cpu.Y);
			if (!forWriteOnly) (emu->cpu.value)=mmc::read(emu, emu->cpu.addr);
			break;

		case opcode::ADR_IND:
			// Indirect Absolute mode. Find the 16-bit address contained
			// at the given location.
			(emu->cpu.addr)=mmc::fetchWordOperand(emu, emu->cpu.PC);
			emu->cpu.addr=makeWord(mmc::read(emu, emu->cpu.addr), mmc::read(emu, maddr_t(((valueOf(emu->cpu.addr)+1)&0x00FF)|(valueOf(emu->cpu.addr)&0xFF00))));
			if (!forWriteOnly) (emu->cpu.value)=mmc::read(emu, emu->cpu.addr);
			break;

		default:
			FATAL_ERROR(INVALID_INSTRUCTION, INVALID_ADDRESS_MODE, "opcode", code, "instruction", op.inst, "adrmode", op.addrmode);
			break;
		}

		return cycles;
	}

	static bool execute(emu::CTX *emu, const opcode::M6502_OPCODE op, bool& writeBack, int& cycles)
	{
		switch (op.inst)
		{
		// arithmetic
		case opcode::INS_ADC: // Add with carry.
			arithmetic::ADC(emu);
			break;

		case opcode::INS_SBC: // Subtract
			arithmetic::SBC(emu);
			break;
			
		case opcode::INS_INC: // Increment memory by one
			inc(M);
			status::setNZ(emu, M);
			writeBack = true;
			break;

		case opcode::INS_DEC: // Decrement memory by one
			dec(M);
			status::setNZ(emu, M);
			writeBack = true;
			break;

		case opcode::INS_DEX: // Decrement index X by one
			dec(regX);
			status::setNZ(emu, regX);
			break;

		case opcode::INS_DEY: // Decrement index Y by one
			dec(regY);
			status::setNZ(emu, regY);
			break;

		case opcode::INS_INX: // Increment index X by one
			inc(regX);
			status::setNZ(emu, regX);
			break;

		case opcode::INS_INY: // Increment index Y by one
			inc(regY);
			status::setNZ(emu, regY);
			break;

		// bit manipulation
		case opcode::INS_AND: // AND memory with accumulator.
			status::setNZ(emu, regA&=M);
			break;

		case opcode::INS_ASLA: // Shift left one bit
			bitshift::ASL(emu, regA);
			break;

		case opcode::INS_ASL:
			bitshift::ASL(emu, M);
			writeBack = true;
			break;

		case opcode::INS_EOR: // XOR Memory with accumulator, store in accumulator
			status::setNZ(emu, regA^=M);
			break;

		case opcode::INS_LSR: // Shift right one bit
			bitshift::LSR(emu, M);
			writeBack = true;
			break;

		case opcode::INS_LSRA:
			bitshift::LSR(emu, regA);
			break;

		case opcode::INS_ORA: // OR memory with accumulator, store in accumulator.
			status::setNZ(emu, regA|=M);
			break;

		case opcode::INS_ROL: // Rotate one bit left
			bitshift::ROL(emu, M);
			writeBack = true;
			break;

		case opcode::INS_ROLA:
			bitshift::ROL(emu, regA);
			break;

		case opcode::INS_ROR: // Rotate one bit right
			bitshift::ROR(emu, M);
			writeBack = true;
			break;

		case opcode::INS_RORA:
			bitshift::ROR(emu, regA);
			break;

		// branch
		case opcode::INS_JMP: // Jump to new location
			(emu->cpu.PC)=(emu->cpu.addr);
			break;

		case opcode::INS_JSR: // Jump to new location, saving return address. Push return address on stack
			dec(emu->cpu.PC);
			stack::pushPC(emu);
			(emu->cpu.PC)=(emu->cpu.addr);
			break;
		
		case opcode::INS_RTS: // Return from subroutine. Pull PC from stack.
			(emu->cpu.PC)=stack::popWord(emu);
			inc(emu->cpu.PC);
			break;

		case opcode::INS_BCC: // Branch on carry clear
			if (!(emu->cpu.P)[F_CARRY])
			{
jBranch:
				cycles+=((valueOf(emu->cpu.PC)^valueOf(emu->cpu.addr))&0xFF00)?2:1;
				(emu->cpu.PC)=(emu->cpu.addr);
			}
			break;

		case opcode::INS_BCS: // Branch on carry set
			if ((emu->cpu.P)[F_CARRY]) goto jBranch;else break;
		case opcode::INS_BEQ: // Branch on zero
			if ((emu->cpu.P)[F_ZERO]) goto jBranch;else break;
		case opcode::INS_BMI: // Branch on negative result
			if ((emu->cpu.P)[F_SIGN]) goto jBranch;else break;
		case opcode::INS_BNE: // Branch on not zero
			if (!(emu->cpu.P)[F_ZERO]) goto jBranch;else break;
		case opcode::INS_BPL: // Branch on positive result
			if (!(emu->cpu.P)[F_SIGN]) goto jBranch;else break;
		case opcode::INS_BVC: // Branch on overflow clear
			if (!(emu->cpu.P)[F_OVERFLOW]) goto jBranch;else break;
		case opcode::INS_BVS: // Branch on overflow set
			if (emu->cpu.P[F_OVERFLOW]) goto jBranch;else break;

		// interrupt
		case opcode::INS_BRK: // Break
			inc(emu->cpu.PC);
			interrupt::request(emu, IRQTYPE::BRK);
			break;

		case opcode::INS_RTI: // Return from interrupt. Pull status and PC from stack.
			emu->cpu.P.asBitField()=stack::popByte(emu);
			(emu->cpu.P)|=F_RESERVED;
			emu->cpu.PC=stack::popWord(emu);
			break;

		// set/clear flag
		case opcode::INS_CLC: // Clear carry flag
			emu->cpu.P.clear(F_CARRY);
			break;
		case opcode::INS_CLD: // Clear decimal flag
			emu->cpu.P.clear(F_DECIMAL);
			break;
		case opcode::INS_CLI: // Clear interrupt flag
			emu->cpu.P.clear(F_INTERRUPT_OFF);
			break;
		case opcode::INS_CLV: // Clear overflow flag
			emu->cpu.P.clear(F_OVERFLOW);
			break;
		case opcode::INS_SEC: // Set carry flag
			emu->cpu.P.set(F_CARRY);
			break;
		case opcode::INS_SED: // Set decimal flag
			emu->cpu.P.set(F_DECIMAL);
			break;
		case opcode::INS_SEI: // Set interrupt disable status
			emu->cpu.P.set(F_INTERRUPT_OFF);
			break;

		// compare
		case opcode::INS_BIT:
			status::setNV(emu, M);
			status::setZ(emu, M&=(emu->cpu.A));
			break;

		case opcode::INS_CMP: // Compare memory and accumulator
		case opcode::INS_CPX: // Compare memory and index X
		case opcode::INS_CPY: // Compare memory and index Y
			switch (op.inst)
			{
			case opcode::INS_CMP:
				emu->cpu.temp=emu->cpu.A;break;
			case opcode::INS_CPX:
				emu->cpu.temp=emu->cpu.X;break;
			case opcode::INS_CPY:
				emu->cpu.temp=emu->cpu.Y;break;
			default:
				break;
			}
			emu->cpu.temp=(emu->cpu.temp)+0x100-(emu->cpu.value);
			// if (temp>0xFF) [R]-[M]>=0 C=1;
			emu->cpu.P.change<F_CARRY>(SUM.overflow());
			emu->cpu.temp=((emu->cpu.temp)-0x100)&0xFF;
			status::setNZ(emu, SUM);
			break;

		// load/store
		case opcode::INS_LDA: // Load accumulator with memory
			status::setNZ(emu, M);
			emu->cpu.A=emu->cpu.value;
			break;

		case opcode::INS_LDX: // Load index X with memory
			status::setNZ(emu, M);
			emu->cpu.X=emu->cpu.value;
			break;

		case opcode::INS_LDY: // Load index Y with memory
			status::setNZ(emu, M);
			emu->cpu.Y=emu->cpu.value;
			break;

		case opcode::INS_STA: // Store accumulator in memory
			emu->cpu.value = emu->cpu.A;
			writeBack = true;
			break;

		case opcode::INS_STX: // Store index X in memory
			emu->cpu.value = emu->cpu.X;
			writeBack = true;
			break;

		case opcode::INS_STY: // Store index Y in memory
			emu->cpu.value = emu->cpu.Y;
			writeBack = true;
			break;

		// stack
		case opcode::INS_PHA: // Push accumulator on stack
			stack::pushReg(emu, regA);
			break;

		case opcode::INS_PHP: // Push processor status on stack
			stack::pushReg(emu, emu->cpu.P);
			break;

		case opcode::INS_PLA: // Pull accumulator from stack
			emu->cpu.A=stack::popByte(emu);
			status::setNZ(emu, regA);
			break;

		case opcode::INS_PLP: // Pull processor status from stack
			emu->cpu.P.asBitField()=stack::popByte(emu);
			(emu->cpu.P)|=F_RESERVED;
			break;
		
		// transfer
		case opcode::INS_TAX: // Transfer accumulator to index X
			emu->cpu.X=emu->cpu.A;
			status::setNZ(emu, regX);
			break;
		case opcode::INS_TAY: // Transfer accumulator to index Y
			emu->cpu.Y=emu->cpu.A;
			status::setNZ(emu, regY);
			break;
		case opcode::INS_TSX: // Transfer stack pointer to index X
			emu->cpu.X=valueOf(emu->cpu.SP);
			status::setNZ(emu, regX);
			break;
		case opcode::INS_TXA: // Transfer index X to accumulator
			emu->cpu.A=emu->cpu.X;
			status::setNZ(emu, regA);
			break;
		case opcode::INS_TXS: // Transfer index X to stack pointer
			emu->cpu.SP=emu->cpu.X;
			break;
		case opcode::INS_TYA: // Transfer index Y to accumulator
			emu->cpu.A=emu->cpu.Y;
			status::setNZ(emu, regA);
			break;

		// other
		case opcode::INS_NOP: break; // No OPeration

		// unofficial

		default:
			return false;
		}
		// success
		return true;
	}

	int nextInstruction(emu::CTX *emu)
	{
		int cycles = 0;
		// handle interrupt request
		interrupt::poll(emu);

		// step1: fetch instruction
		if (emu->cpu.PC.zero())
		{
			// program terminates
			assert(emu->cpu.SP.reachMax());
			return -1;
		}

#ifdef WANT_RUN_HIT
		emu->cpu.instructionHit[(emu->cpu.PC)&0x7FFF]=true;
#endif

		const maddr_t opaddr = emu->cpu.PC;
		const opcode_t opcode = mmc::fetchOpcode(emu, emu->cpu.PC);

		// step2: decode
		const opcode::M6502_OPCODE op = opcode::decode(emu, opcode);
		ERROR_UNLESS(opcode::usual(emu, opcode), INVALID_INSTRUCTION, INVALID_OPCODE, "opaddr", valueOf(opaddr), "opcode", opcode, "instruction", op.inst);

		// step3: read effective address & operands
		cycles += readEffectiveAddress(emu, opcode, op, (op.inst== opcode::INS_STA || op.inst==opcode::INS_STX || op.inst==opcode::INS_STY));
		assert((valueOf(emu->cpu.PC)-valueOf(opaddr)) == op.size);

#ifdef WANT_DISASSEMBLY
		debug::printDisassembly(emu, opaddr, opcode, emu->cpu.X, emu->cpu.Y, emu->cpu.EA, emu->cpu.M);
#endif

		// step4: execute
		bool writeBack = false;
		if (!execute(emu, op, writeBack, cycles))
		{
			// execution failed
			FATAL_ERROR(INVALID_INSTRUCTION, INVALID_OPCODE, "opaddr", valueOf(opaddr), "opcode", opcode, "instruction", op.inst);
			return -1;
		}

		// step5: write back (when needed)
		if (writeBack)
		{
			assert((emu->cpu.addr) != 0xCCCC);
			mmc::write(emu, emu->cpu.addr, emu->cpu.value);
		}

		cycles += op.cycles;
		// end of instruction pipeline

		assert((emu->cpu.P)[F_RESERVED]);
#ifdef MONITOR_CPU
		debug::printCPUState(emu->cpu.PC, emu->cpu.A, emu->cpu.X ,emu->cpu.Y, valueOf(emu->cpu.P), emu->cpu.SP, cycles);
#endif

		// update statistics
		STAT_ADD(emu->cpu.totInstructions, 1);
		STAT_ADD((emu->cpu.numInstructionsPerOpcode)[(int)op.inst], 1);
		STAT_ADD((emu->cpu.numInstructionsPerAdrMode)[(int)op.addrmode], 1);
		STAT_ADD((emu->cpu.totCycles), cycles);
		(emu->cpu.remainingCycles) -= cycles;
		return cycles;
	}
}
}
