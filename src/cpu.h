#pragma once

#include "opcodes.h"
#include <stdio.h>

namespace nes
{
namespace emu
{
    struct CTX;
}

// status flags
enum PSW
{
    F_CARRY=0x1,
    F_ZERO=0x2,
    F_INTERRUPT_OFF=0x4,
    F_BCD=0x8,
    F_DECIMAL=F_BCD,
    F_BREAK=0x10,
    F_RESERVED=0x20, // not used (always set)
    F_OVERFLOW=0x40,
    F_NEGATIVE=0x80,
    F_SIGN=F_NEGATIVE,
    F__NV=F_NEGATIVE|F_OVERFLOW
};

// irq types
enum class IRQTYPE
{
    NONE=0x0,
    NMI=0x1,
    BRK=0x2,
    IRQ=0x4,
    RST=0x8
};

// alias of registers with wrapping
typedef bit_field<_reg8_t, 8> reg_bit_field_t;
#define regA fast_cast((emu->cpu.A), reg_bit_field_t)
#define regX fast_cast((emu->cpu.X), reg_bit_field_t)
#define regY fast_cast((emu->cpu.Y), reg_bit_field_t)
#define M fast_cast((emu->cpu.value), operandb_t)
#define SUM fast_cast((emu->cpu.temp), alu_t)

namespace stack
{
    inline void pushByte(emu::CTX *emu, const byte_t byte);

    inline void pushReg(emu::CTX *emu, const reg_bit_field_t& reg);

    inline void pushWord(emu::CTX *emu, const word_t word);

    inline void pushPC(emu::CTX *emu);

    inline byte_t popByte(emu::CTX *emu);

    inline word_t popWord(emu::CTX *emu);

    void reset(emu::CTX *emu);
}

namespace cpu
{
    struct CTX
    {
        // Register file
#ifdef __declspec
        __declspec(align(32)) // try to make registers fit into a cache line of host CPU
#endif
        _reg8_t                     A; // accumulator
        _reg8_t                     X, Y; // index
        maddr8_t                    SP; // stack pointer
        flag_set<_reg8_t, PSW, 8>   P; // status
        maddr_t                     PC; // program counter

        maddr_t                     addr; // effective address
#define EA (emu->cpu.addr)

        byte_t                      value; // operand
        _alutemp_t                  temp;

        // Interrupts
        flag_set<_reg8_t, IRQTYPE, 8> pendingIRQs;

        // Run-time statistics
        long remainingCycles;
#ifdef WANT_STATISTICS
        uint64_t totInstructions;
        uint64_t totCycles;
        uint64_t totInterrupts;
        uint64_t numInstructionsPerOpcode[(int)opcode::_INS_MAX];
        uint64_t numInstructionsPerAdrMode[(int)opcode::_ADR_MAX];
#define STAT_ADD(VAR, INC) VAR += INC
#else
#define STAT_ADD(VAR, INC) (void)0
#endif

        // Crazy debugging
#ifdef WANT_RUN_HIT
        bool instructionHit[0x8000];
#endif
    };

    // global functions
    void reset(emu::CTX *emu);

    void irq(emu::CTX *emu, const IRQTYPE type);

    int nextInstruction(emu::CTX *emu);
    bool run(emu::CTX *emu, int n, long cycles);

    // debug
    void dump(emu::CTX *emu);

    // save state
    void save(emu::CTX *emu, FILE *fp);
    void load(emu::CTX *emu, FILE *fp);
}
}
