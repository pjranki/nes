#pragma once

#include "types.h"
#include "internals.h"

#define BUTTON_A 0
#define BUTTON_B 1
#define BUTTON_SELECT 2
#define BUTTON_START 3
#define BUTTON_UP 4
#define BUTTON_DOWN 5
#define BUTTON_LEFT 6
#define BUTTON_RIGHT 7
#define BUTTON_COUNT 8

namespace nes
{

namespace ui
{
    typedef void (NESAPI *renderLineFn)(void *ctx, const int y, const palindex_t line[], const int line_size, const rgb16_t *palette);

    typedef bool (NESAPI *hasInputfn)(void *ctx, const int player);
    typedef void (NESAPI *resetInputfn)(void *ctx);
    typedef int (NESAPI *readInputfn)(void *ctx, const int player);

    typedef bool (NESAPI *forceTerminatefn)(void *ctx);

    typedef void (NESAPI *limitFPSfn)(void *ctx);

    typedef void (NESAPI *onFrameBeginfn)(void *ctx);
    typedef void (NESAPI *onFrameEndfn)(void *ctx);

    typedef void (NESAPI *doEventsfn)(void *ctx);

    struct CTX
    {
        void *ctx;
        renderLineFn renderLine;
        hasInputfn hasInput;
        resetInputfn resetInput;
        readInputfn readInput;
        forceTerminatefn forceTerminate;
        limitFPSfn limitFPS;
        onFrameBeginfn onFrameBegin;
        onFrameEndfn onFrameEnd;
        doEventsfn doEvents;
    };
}
}
