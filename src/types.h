#pragma once

// Debug|Win32
#if defined(DEBUG) || defined(_DEBUG)
#define DEBUG_CONFIGURATION
#else
#define RELEASE_CONFIGURATION
#endif

#ifdef DEBUG_CONFIGURATION
#define FAST_TYPE
#define VERBOSE
#define FORCE_SAFE_CASTING
#define WANT_STATISTICS
#define WANT_MEM_PROTECTION
#define SHOW_240_LINES
#define FPS_LIMIT
#define MONITOR_RENDERING
#endif

#ifdef DEBUGTEST_CONFIGURATION
#define FAST_TYPE
#define VERBOSE
#define FORCE_SAFE_CASTING
#define WANT_STATISTICS
#define ALLOW_ADDRESS_WRAP
#define WANT_RUN_HIT
#define SHOW_240_LINES
#endif

#ifdef RELEASE_CONFIGURATION
#define FAST_TYPE
#define ALLOW_ADDRESS_WRAP
#define FPS_LIMIT
#define PAUSE_WHEN_INACTIVE
#define LEFT_CLIP
#endif

#ifdef _WIN32
#define NESAPI __stdcall
#else
#define NESAPI
#define __forceinline __attribute__((always_inline))
#define size_t uint32_t
#define _CRT_WIDE_(s) L ## s
#define _CRT_WIDE(s) _CRT_WIDE_(s)
#define NES_DONT_STDIO_FLUSH
#endif

#define VERBOSE

#include <cstdint>

//FAST_TYPE; VERBOSE; FORCE_SAFE_CASTING; WANT_STATISTICS; WANT_MEM_PROTECTION; SHOW_240_LINES; WANT_DX9; FPS_LIMIT; MONITOR_RENDERING;
// Debug|x64
//FAST_TYPE; VERBOSE; FORCE_SAFE_CASTING; WANT_STATISTICS; WANT_MEM_PROTECTION; SHOW_240_LINES; WANT_DX9; FPS_LIMIT; MONITOR_RENDERING;
// DebugTest|Win32
//FAST_TYPE; VERBOSE; FORCE_SAFE_CASTING; WANT_STATISTICS; ALLOW_ADDRESS_WRAP; WANT_RUN_HIT; SHOW_240_LINES;
// DebugTest|x64
//FAST_TYPE; VERBOSE; FORCE_SAFE_CASTING; WANT_STATISTICS; ALLOW_ADDRESS_WRAP; WANT_RUN_HIT; SHOW_240_LINES;
// Release|Win32
//FAST_TYPE; ALLOW_ADDRESS_WRAP; FPS_LIMIT; WANT_DX9; PAUSE_WHEN_INACTIVE; LEFT_CLIP;
// Release|x64
//FAST_TYPE; ALLOW_ADDRESS_WRAP; FPS_LIMIT; WANT_DX9; PAUSE_WHEN_INACTIVE;

// forward declaration
namespace nes
{
    template <typename T, int bits> class bit_field;
    template <typename T, typename ET, int bits> class flag_set;
}

#include "valueobj.h"
#include "bitfield.h"
#include "flagset.h"
