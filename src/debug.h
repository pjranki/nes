#pragma once

#include "internals.h"
#include <stdio.h>

namespace nes
{
namespace emu
{
    struct CTX;
}

namespace debug
{
#define NES_MAX_PRINTF_MESSAGE 511
    typedef void (NESAPI *printfFn)(void *ctx, const char *message);

    struct CTX {
        void *ctx;
        printfFn print;
    };

    void info(emu::CTX *emu, const char *format, ...);

	void warn(emu::CTX *emu, EMUERROR, EMUERRORSUBTYPE, const char *, unsigned long, ...);

	void error(emu::CTX *emu, EMUERROR, EMUERRORSUBTYPE, const wchar_t *, const char *, unsigned long, ...);
	void fatalError(emu::CTX *emu, EMUERROR, EMUERRORSUBTYPE, const wchar_t *, const char *, unsigned long, ...);

	void printDisassembly(emu::CTX *emu, const maddr_t pc, const opcode_t opcode, const _reg8_t rx, const _reg8_t ry, const maddr_t addr, const operand_t operand);
	void printCPUState(emu::CTX *emu, const maddr_t pc, const _reg8_t ra, const _reg8_t rx, const _reg8_t ry, const _reg8_t rp, const _reg8_t rsp, const int cyc);
	void printPPUState(emu::CTX *emu, const long long frameNum, const int scanline, const bool vblank, const bool hit, const bool bgmsk, const bool sprmsk);
}
}

#ifdef _WIN32

#define WARN(TYPE, SUBTYPE, ...) debug::warn(emu, TYPE, SUBTYPE, __FUNCTION__, __LINE__, __VA_ARGS__, 0)
#define WARN_IF(E, TYPE, SUBTYPE, ...) if (E) WARN(TYPE, SUBTYPE, __VA_ARGS__)

#define ERROR(TYPE, SUBTYPE, ...) debug::error(emu, TYPE, SUBTYPE, _CRT_WIDE(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__, 0)
#define ERROR_IF(E, TYPE, SUBTYPE, ...) if (E) ERROR(TYPE, SUBTYPE, __VA_ARGS__)
#define ERROR_UNLESS(E, TYPE, SUBTYPE, ...) if (!(E)) ERROR(TYPE, SUBTYPE, __VA_ARGS__)

#define FATAL_ERROR(TYPE, SUBTYPE, ...) debug::fatalError(emu, TYPE, SUBTYPE, _CRT_WIDE(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__, 0)
#define FATAL_ERROR_IF(E, TYPE, SUBTYPE, ...) if (E) FATAL_ERROR(TYPE, SUBTYPE, __VA_ARGS__)
#define FATAL_ERROR_UNLESS(E, TYPE, SUBTYPE, ...) if (!(E)) FATAL_ERROR(TYPE, SUBTYPE, __VA_ARGS__)

#else

#define WARN(TYPE, SUBTYPE, ...) debug::warn(emu, TYPE, SUBTYPE, __FUNCTION__, __LINE__, ## __VA_ARGS__, 0)
#define WARN_IF(E, TYPE, SUBTYPE, ...) if (E) WARN(TYPE, SUBTYPE, ## __VA_ARGS__)

#define ERROR(TYPE, SUBTYPE, ...) debug::error(emu, TYPE, SUBTYPE, _CRT_WIDE(__FILE__), __FUNCTION__, __LINE__, ## __VA_ARGS__, 0)
#define ERROR_IF(E, TYPE, SUBTYPE, ...) if (E) ERROR(TYPE, SUBTYPE, ## __VA_ARGS__)
#define ERROR_UNLESS(E, TYPE, SUBTYPE, ...) if (!(E)) ERROR(TYPE, SUBTYPE, ## __VA_ARGS__)

#define FATAL_ERROR(TYPE, SUBTYPE, ...) debug::fatalError(emu, TYPE, SUBTYPE, _CRT_WIDE(__FILE__), __FUNCTION__, __LINE__, ## __VA_ARGS__, 0)
#define FATAL_ERROR_IF(E, TYPE, SUBTYPE, ...) if (E) FATAL_ERROR(TYPE, SUBTYPE, ## __VA_ARGS__)
#define FATAL_ERROR_UNLESS(E, TYPE, SUBTYPE, ...) if (!(E)) FATAL_ERROR(TYPE, SUBTYPE, ## __VA_ARGS__)

#endif
