#ifndef _H_NES_H_
#define _H_NES_H_

#include <cstring>
#include <functional>

#include "nes_emu.h"

namespace nes {

    class Emulator
    {
    public:
        typedef std::function<void(uint32_t y, const nes::palindex_t *line, uint32_t line_size, const nes::rgb16_t *palette)> RenderLineFn;
        typedef std::function<void(void)> FrameStartFn;
        typedef std::function<void(void)> FrameEndFn;
        typedef std::function<void(const char *)> LogFn;
        typedef std::function<void(void *, const void *, uint32_t)> MemcpyFn;

        Emulator() :
                render_line_(nullptr),
                frame_start_(nullptr),
                frame_end_(nullptr),
                log_(nullptr),
                memcpy_(nullptr) {
            std::memset(&emu_, 0, sizeof(emu_));
            emu_.debug.ctx = this;
            emu_.debug.print = customPrint;
            emu_.rom.ctx = this;
            emu_.rom.imgcpy = customMemcpy;
            emu_.ui.ctx = this;
            emu_.ui.renderLine = renderLine;
            emu_.ui.hasInput = hasInput;
            emu_.ui.resetInput = resetInput;
            emu_.ui.readInput = readInput;
            emu_.ui.forceTerminate = forceTerminate;
            emu_.ui.limitFPS = limitFPS;
            emu_.ui.onFrameBegin = onFrameBegin;
            emu_.ui.onFrameEnd = onFrameEnd;
            emu_.ui.doEvents = doEvents;
            nes::emu::init(&emu_);
        }
        ~Emulator() {
            nes::emu::deinit(&emu_);
        }
        void set_render_line(RenderLineFn func) { render_line_ = func; }
        void set_frame_start(FrameStartFn func) { frame_start_ = func; }
        void set_frame_end(FrameEndFn func) { frame_end_ = func; }
        void set_log(LogFn func) { log_ = func; }
        void set_memcpy(MemcpyFn func) { memcpy_ = func; }
        void reset() {
            nes::emu::reset(&emu_);
        }
        bool load(const void *rom, uint32_t size) {
            if (nes::emu::load(&emu_, (uint8_t*)rom, (uint32_t)size)) {
                if (nes::emu::setup(&emu_)) {
                    return true;
                }
            }
            return false;
        }
        bool tick() {
            return nes::emu::tick(&emu_);
        }
    private:
        static void NESAPI renderLine(void *ctx, const int y, const nes::palindex_t line[], const int line_size, const nes::rgb16_t *palette);
        static bool NESAPI hasInput(void *ctx, const int player);
        static void NESAPI resetInput(void *ctx);
        static int NESAPI readInput(void *ctx, const int player);
        static bool NESAPI forceTerminate(void *ctx);
        static void NESAPI limitFPS(void *ctx);
        static void NESAPI onFrameBegin(void *ctx);
        static void NESAPI onFrameEnd(void *ctx);
        static void NESAPI doEvents(void *ctx);
        static void NESAPI customPrint(void *ctx, const char *message);
        static void NESAPI customMemcpy(void *ctx, void *dst, void *src, uint32_t size);
        RenderLineFn render_line_;
        FrameStartFn frame_start_;
        FrameEndFn frame_end_;
        LogFn log_;
        MemcpyFn memcpy_;
        nes::emu::CTX emu_;
    };
}

#endif
