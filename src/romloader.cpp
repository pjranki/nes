#include "macros.h"
#include "types.h"
#include "internals.h"
#include "debug.h"
#include "rom.h"
#include "types.h"
#include "nes_emu.h"
#include <string.h>

namespace nes
{
namespace rom
{
    inline static uint32_t memread(emu::CTX *emu, void *dest, uint32_t size, uint32_t count, uint8_t **fp)
    {
        emu->rom.imgcpy(emu->rom.ctx, dest, *fp, size*count);
        *fp += (size*count);
        return size*count;
    }

#define NES_ROM_FREAD(output,read_size,count,rom_image_ptr) memread(emu,output,read_size,count,&rom_image_ptr)

	bool load(emu::CTX *emu, uint8_t *rom_image, uint32_t rom_size)
	{
        uint8_t *fp = rom_image;
		uint8_t nesMagic[4]={0};
		uint8_t reserved[8]={0};

        debug::info(emu, "image is %d bytes at 0x%p\n", rom_size, rom_image);

		// check signature
        NES_ROM_FREAD(nesMagic,4,1,fp);
		if (memcmp(nesMagic,"NES",3))
		{
			ERROR(INVALID_ROM, INVALID_FILE_SIGNATURE);
            return false;
		}

		debug::info(emu, "[-] loading...\n");

        NES_ROM_FREAD(&(emu->rom.prgCount),1,1,fp);
        NES_ROM_FREAD(&(emu->rom.chrCount),1,1,fp);
        NES_ROM_FREAD(&(emu->rom.romCtrl),1,1,fp);
        NES_ROM_FREAD(&(emu->rom.romCtrl2),1,1,fp);
        NES_ROM_FREAD(&reserved,8,1,fp);

        debug::info(emu, "[ ] %u * 16K ROM Banks\n", (emu->rom.prgCount));
        debug::info(emu, "[ ] %u * 8K CHR Banks\n", (emu->rom.chrCount));
        debug::info(emu, "[ ] ROM Control Byte #1 : %u\n", valueOf(emu->rom.romCtrl));
        debug::info(emu, "[ ] ROM Control Byte #2 : %u\n", valueOf(emu->rom.romCtrl2));

        emu->rom.mapper=emu->rom.romCtrl(ROMCONTROL1::MAPPERLOW);
        emu->rom.mapper|= emu->rom.romCtrl2(ROMCONTROL2::MAPPERHIGH)<<4;
        debug::info(emu, "[ ] Mapper : #%u\n", emu->rom.mapper);

        emu->rom.mirroring=emu->rom.romCtrl[ROMCONTROL1::VERTICALM]?MIRRORING::VERTICAL:MIRRORING::HORIZONTAL;
		if (emu->rom.romCtrl[ROMCONTROL1::FOURSCREEN]) emu->rom.mirroring=MIRRORING::FOURSCREEN;

        debug::info(emu, "[ ] Mirroring type : %u\n", emu->rom.mirroring);
        debug::info(emu, "[ ] Fourscreen mode : %u\n", emu->rom.romCtrl[ROMCONTROL1::FOURSCREEN]);
        debug::info(emu, "[ ] Trainer data present : %u\n", emu->rom.romCtrl[ROMCONTROL1::TRAINER]);
        debug::info(emu, "[ ] SRAM present : %u\n", emu->rom.romCtrl[ROMCONTROL1::BATTERYPACK]);
    
		// read trainer data (if present)
		if (emu->rom.romCtrl[ROMCONTROL1::TRAINER])
		{
            emu->rom.trainerSize = 512;
            emu->rom.trainerData = (char*)fp;
            fp += 512;
		}

		debug::info(emu, "[ ] reading ROM image...\n");
		// read rom image
        emu->rom.imageSize = (emu->rom.prgCount)*0x4000;
        emu->rom.imageData = (char*)fp;
        fp += (0x4000 * emu->rom.prgCount);

		debug::info(emu, "[ ] reading VROM data...\n");
		// read VROM
        emu->rom.vromSize = emu->rom.chrCount*0x2000;
        emu->rom.vromData = (char*)fp;
        fp += (0x2000 * emu->rom.chrCount);

		// done. close file
		debug::info(emu, "[-] loaded!\n");
		return true;
	}

	void unload(emu::CTX *emu)
	{
        // nothing to do
	}

	int mapperType(emu::CTX *emu)
	{
		return emu->rom.mapper;
	}

	MIRRORING mirrorMode(emu::CTX *emu)
	{
		return emu->rom.mirroring;
	}

	void setMirrorMode(emu::CTX *emu, MIRRORING newMode)
	{
        emu->rom.mirroring = newMode;
	}

	const char* getImage(emu::CTX *emu)
	{
		return emu->rom.imageData;
	}

	const char* getVROM(emu::CTX *emu)
	{
		return emu->rom.vromData;
	}

	size_t sizeOfImage(emu::CTX *emu)
	{
		return emu->rom.imageSize;
	}

	int count16KPRG(emu::CTX *emu)
	{
		return emu->rom.prgCount;
	}

	int count8KPRG(emu::CTX *emu)
	{
		return emu->rom.prgCount*2;
	}

	size_t sizeOfVROM(emu::CTX *emu)
	{
		return emu->rom.vromSize;
	}

	int count8KCHR(emu::CTX *emu)
	{
		return emu->rom.chrCount;
	}

	int count4KCHR(emu::CTX *emu)
	{
		return emu->rom.chrCount*2;
	}
}
}
