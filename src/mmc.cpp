#include "macros.h"
#include "types.h"
#include "internals.h"
#include "debug.h"
#include "rom.h"
#include "mmc.h"
#include "mmc_template.h"
#include "cpu.h"
#include "ppu.h"
#include "nes_emu.h"
#include <string.h>

namespace nes
{
namespace mmc
{
    inline __forceinline uint8_t cache_lookup_byte(emu::CTX *emu, uint16_t addr, uint8_t *bank, uint16_t bank_adjustment, uint8_t *view_cache, uint16_t view_cache_size, uint16_t view_count)
    {
        uint8_t value = 0;
        uint16_t view = 0;
        uint16_t view_no = 0;
        uint16_t view_header_size = 0;
        uint16_t view_size = 0;
        uint16_t view_sel_no = 0;
        uint16_t view_sel_addr = 0;
        uint16_t *view_addr = (uint16_t*)view_cache;

        // no cache? read the byte
        if (nullptr == view_cache)
        {
            emu->rom.imgcpy(emu->rom.ctx, &value, &(bank[addr - bank_adjustment]), 1);
            return value;
        }

        // first part of the cache stores the NES address of each view.
        // rest of cache is evenly divided between views as space.
        // views are kept in address order, lowest to highest address.
        view_header_size = view_count * sizeof(uint16_t);
        view_size = (view_cache_size - view_header_size) / view_count;

        // find an existing view of the ram
        for (view_no = 0; view_no < view_count; view_no++)
        {
            view = view_addr[view_no];
            if ((addr >= view) && (addr < (view + view_size)))
            {
                // found, break with view no
                break;
            }
        }

        // did we find an existing view of the ram?
        if (view_no < view_count)
        {
            // found, select this view
            view_sel_no = view_no;
            view_sel_addr = view;
        }
        else
        {
            // nope, cache it

            // where to cache it (TODO: make this smarter)
            if (((addr - bank_adjustment) + view_size) >= 0x2000)
            {
                view_sel_no = view_count - 1;  // use the last view ALWAYS
                view_sel_addr = (0x2000 - view_size) + bank_adjustment;
            }
            else
            {
                view_sel_no = (addr - bank_adjustment) / (0x2000/view_count);
                view_sel_no = view_count - 1; // TODO
                view_sel_addr = addr;
            }

            // debug
            uint16_t old = view_addr[view_sel_no];
            //debug::info(emu, "cache miss u08 v=%02d, moving 0x%04x to 0x%04x\n", view_sel_no, old, view_sel_addr);

            // read to cache
            view_addr[view_sel_no] = view_sel_addr;
            //debug::info(emu, "u08 cache %d bytes from %d to %d\n", view_size, view_sel_addr - bank_adjustment, view_header_size + (view_size * view_sel_no));
            emu->rom.imgcpy(emu->rom.ctx, &(view_cache[view_header_size + (view_size * view_sel_no)]), &(bank[view_sel_addr - bank_adjustment]), view_size);
        }

        // extract value from cache
        //debug::info(emu, "u08 read 1 bytes from %d\n", 1, view_header_size + (view_size * view_sel_no) + (addr - view_sel_addr));
        value = view_cache[view_header_size + (view_size * view_sel_no) + (addr - view_sel_addr)];
        return value;
    }

    inline __forceinline uint16_t cache_lookup_word(emu::CTX *emu, uint16_t addr, uint8_t *bank, uint16_t bank_adjustment, uint8_t *view_cache, uint16_t view_cache_size, uint16_t view_count)
    {
        uint16_t value = 0;
        uint16_t view = 0;
        uint16_t view_no = 0;
        uint16_t view_header_size = 0;
        uint16_t view_size = 0;
        uint16_t view_sel_no = 0;
        uint16_t view_sel_addr = 0;
        uint16_t *view_addr = (uint16_t*)view_cache;

        // no cache? read the byte
        if (nullptr == view_cache || 1)
        {
            emu->rom.imgcpy(emu->rom.ctx, &value, &(bank[addr - bank_adjustment]), 2);
            value = getWord(&value);
            return value;
        }

        // first part of the cache stores the NES address of each view.
        // rest of cache is evenly divided between views as space.
        // views are kept in address order, lowest to highest address.
        view_header_size = view_count * sizeof(uint16_t);
        view_size = (view_cache_size - view_header_size) / view_count;

        // find an existing view of the ram
        for (view_no = 0; view_no < view_count; view_no++)
        {
            view = view_addr[view_no];
            if ((addr >= view) && (addr < (view + view_size)))
            {
                // found, break with view no
                break;
            }
        }

        // did we find an existing view of the ram?
        if (view_no < view_count)
        {
            // found, select this view
            view_sel_no = view_no;
            view_sel_addr = view;
        }
        else
        {
            // nope, cache it

            // where to cache it
            if (((addr - bank_adjustment) + view_size) >= 0x2000)
            {
                view_sel_no = view_count - 1;  // use the last view ALWAYS
                view_sel_addr = (0x2000 - view_size) + bank_adjustment;
            }
            else
            {
                view_sel_no = (addr - bank_adjustment) / (0x2000 / view_count);
                view_sel_no = view_count - 1; // TODO
                view_sel_addr = addr;
            }

            // debug
            uint16_t old = view_addr[view_sel_no];
            debug::info(emu, "cache miss u16 v=%02d, moving 0x%04x to 0x%04x\n", view_sel_no, old, view_sel_addr);

            // read to cache
            view_addr[view_sel_no] = view_sel_addr;
            //debug::info(emu, "u16 cache %d bytes from %d to %d\n", view_size, view_sel_addr - bank_adjustment, view_header_size + (view_size * view_sel_no));
            emu->rom.imgcpy(emu->rom.ctx, &(view_cache[view_header_size + (view_size * view_sel_no)]), &(bank[view_sel_addr - bank_adjustment]), view_size);
        }

        // extract value from cache
        //debug::info(emu, "u16 read 2 bytes from %d\n", 1, view_header_size + (view_size * view_sel_no) + (addr - view_sel_addr));
        value = getWord(view_cache + (view_header_size + (view_size * view_sel_no) + (addr - view_sel_addr)));
        return value;
    }

    uint8_t RAM::data_byte(emu::CTX *emu, const size_t ptr)
    {
        uint8_t value = 0;
        vassert(ptr<0x800 || ptr >= 0x6000);
        if (ptr < 0x800)
        {
            return ((uint8_t*)this)[ptr];
        }
        else if ((ptr >= 0x6000) && (ptr < 0x8000))
        {
            return this->bank6[ptr - 0x6000];
        }
        else if ((ptr >= 0x8000) && (ptr < 0xA000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bank8[ptr - 0x8000]), 1);
            value = cache_lookup_byte(emu, ptr, this->bank8, 0x8000, emu->mmc.cache.bank8_cache, emu->mmc.cache.bank8_cache_size, emu->mmc.cache.bank8_view_count);
        }
        else if ((ptr >= 0xA000) && (ptr < 0xC000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bankA[ptr - 0xA000]), 1);
            value = cache_lookup_byte(emu, ptr, this->bankA, 0xA000, emu->mmc.cache.bankA_cache, emu->mmc.cache.bankA_cache_size, emu->mmc.cache.bankA_view_count);
        }
        else if ((ptr >= 0xC000) && (ptr < 0xE000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bankC[ptr - 0xC000]), 1);
            value = cache_lookup_byte(emu, ptr, this->bankC, 0xC000, emu->mmc.cache.bankC_cache, emu->mmc.cache.bankC_cache_size, emu->mmc.cache.bankC_view_count);
        }
        else if ((ptr >= 0xE000) && (ptr < 0x10000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bankE[ptr - 0xE000]), 1);
            value = cache_lookup_byte(emu, ptr, this->bankE, 0xE000, emu->mmc.cache.bankE_cache, emu->mmc.cache.bankE_cache_size, emu->mmc.cache.bankE_view_count);
        }
        return value;
    }
    uint16_t RAM::data_word(emu::CTX *emu, const size_t ptr)
    {
        uint16_t value = 0;
        vassert(ptr<0x800 || ptr >= 0x6000);
        if (ptr < 0x800)
        {
            return getWord(&(((byte_t*)this)[ptr]));
        }
        else if ((ptr >= 0x6000) && (ptr < 0x8000))
        {
            return getWord(&(this->bank6[ptr - 0x6000]));
        }
        else if ((ptr >= 0x8000) && (ptr < 0xA000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bank8[ptr - 0x8000]), 2);
            return cache_lookup_word(emu, ptr, this->bank8, 0x8000, emu->mmc.cache.bank8_cache, emu->mmc.cache.bank8_cache_size, emu->mmc.cache.bank8_view_count);
        }
        else if ((ptr >= 0xA000) && (ptr < 0xC000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bankA[ptr - 0xA000]), 2);
            return cache_lookup_word(emu, ptr, this->bankA, 0xA000, emu->mmc.cache.bankA_cache, emu->mmc.cache.bankA_cache_size, emu->mmc.cache.bankA_view_count);
        }
        else if ((ptr >= 0xC000) && (ptr < 0xE000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bankC[ptr - 0xC000]), 2);
            return cache_lookup_word(emu, ptr, this->bankC, 0xC000, emu->mmc.cache.bankC_cache, emu->mmc.cache.bankC_cache_size, emu->mmc.cache.bankC_view_count);
        }
        else if ((ptr >= 0xE000) && (ptr < 0x10000))
        {
            //emu->rom.imgcpy(emu->rom.ctx, &value, &(this->bankE[ptr - 0xE000]), 2);
            return cache_lookup_word(emu, ptr, this->bankE, 0xE000, emu->mmc.cache.bankE_cache, emu->mmc.cache.bankE_cache_size, emu->mmc.cache.bankE_view_count);
        }
        return value;
    }
    uint8_t* RAM::page(const size_t num)
    {
        vassert(num<0x8 || num >= 0x60);
        return &((uint8_t*)this)[num << 8];
    }

	static void updateBank(emu::CTX *emu, uint8_t ** const dest, int& prev, int current)
	{
		// first mask bank the address
		current = mapper::maskPRG(emu, current, rom::count8KPRG(emu));
		assert(current<rom::count8KPRG(emu));

		if (current!=prev)
		{
			// perform update
			//memcpy(dest, rom::getImage(emu)+current*0x2000, 0x2000);
            *dest = (uint8_t*)const_cast<char*>(rom::getImage(emu)) + current * 0x2000;
			prev=current;
		}
	}

	// perform bank switching
	void bankSwitch(emu::CTX *emu, int r8, int rA, int rC, int rE)
	{
		if (r8!=INVALID) updateBank(emu, &(emu->mmc.ram.bank8), emu->mmc.p8, r8);
		if (rA!=INVALID) updateBank(emu, &(emu->mmc.ram.bankA), emu->mmc.pA, rA);
		if (rC!=INVALID) updateBank(emu, &(emu->mmc.ram.bankC), emu->mmc.pC, rC);
		if (rE!=INVALID) updateBank(emu, &(emu->mmc.ram.bankE), emu->mmc.pE, rE);
	}

	void setSRAMEnabled(emu::CTX *emu, bool v)
	{
		emu->mmc.sramEnabled=v;
	}

	void reset(emu::CTX *emu)
	{
		// no prg-rom selected now
		emu->mmc.p8=INVALID;
        emu->mmc.pA=INVALID;
        emu->mmc.pC=INVALID;
        emu->mmc.pE=INVALID;

		// SRAM is disabled by default
        emu->mmc.sramEnabled = false;

		// clear memory
		memset(&(emu->mmc.ram),0,sizeof(emu->mmc.ram));
	}

	void save(emu::CTX *emu, FILE *fp)
	{
		// bank-switching state
		fwrite(&(emu->mmc.p8), sizeof(emu->mmc.p8), 1, fp);
		fwrite(&(emu->mmc.pA), sizeof(emu->mmc.pA), 1, fp);
		fwrite(&(emu->mmc.pC), sizeof(emu->mmc.pC), 1, fp);
		fwrite(&(emu->mmc.pE), sizeof(emu->mmc.pE), 1, fp);

		// data in memory
		fwrite(emu->mmc.ram.bank0, sizeof(emu->mmc.ram.bank0), 1, fp);
		fwrite(emu->mmc.ram.bank6, sizeof(emu->mmc.ram.bank6), 1, fp);

#ifdef SAVE_COMPLETE_MEMORY
		// code in memory
		fwrite(emu->mmc.ram.code, sizeof(emu->mmc.ram.code), 1, fp);
#endif
	}
	
	void load(emu::CTX *emu, FILE *fp)
	{
		// bank-switching state
		int r8, rA, rC, rE;
		fread(&r8, sizeof(r8), 1, fp);
		fread(&rA, sizeof(rA), 1, fp);
		fread(&rC, sizeof(rC), 1, fp);
		fread(&rE, sizeof(rE), 1, fp);

		// data in memory
		fread(emu->mmc.ram.bank0, sizeof(emu->mmc.ram.bank0), 1, fp);
		fread(emu->mmc.ram.bank6, sizeof(emu->mmc.ram.bank6), 1, fp);

#ifdef SAVE_COMPLETE_MEMORY
		// code in memory
		fwrite(emu->mmc.ram.code, sizeof(emu->mmc.ram.code), 1, fp);
        emu->mmc.p8 = r8;
        emu->mmc.pA = rA;
        emu->mmc.pC = rC;
        emu->mmc.pE = rE;
#else
		// restore code
		bankSwitch(emu, r8, rA, rC, rE);
#endif
	}

	byte_t read(emu::CTX *emu, const maddr_t addr)
	{
		byte_t ret=INVALID;
		switch (addr>>13) // bank number/2
		{
		case 0: //[$0000,$2000)
			return emu->mmc.ram.bank0[addr&0x7FF];
		case 1: //[$2000,$4000)
			if (ppu::readPort(emu, addr, ret)) return ret;
			break;
		case 2: //[$4000,$6000)
			switch (valueOf(addr))
			{
			case 0x4015: // APU Register
				return 0;
			case 0x4016: // Input Registers
			case 0x4017:
				if (emu->ui.hasInput(emu->ui.ctx, (addr==0x4017)?1:0))
					return emu->ui.readInput(emu->ui.ctx, (addr==0x4017)?1:0); // outputs button state
				else
					return 0; // joystick not connected
			}
			break;
		case 3:
			return emu->mmc.ram.bank6[addr&0x1FFF];
		case 4:
		case 5:
		case 6:
		case 7:
			//return emu->mmc.ram.bank8[addr-0x8000];
            return emu->mmc.ram.data_byte(emu, addr);
		}
		ERROR(INVALID_MEMORY_ACCESS, MEMORY_CANT_BE_READ, "addr", valueOf(addr));
		return ret;
	}

	void write(emu::CTX *emu, const maddr_t addr, const byte_t value)
	{
		switch (addr>>13) // bank number/2
		{
			case 0: //[$0000,$2000) Internal RAM
                emu->mmc.ram.bank0[addr&0x7FF]=value;
				return;
			case 1: //[$2000,$4000) PPU Registers
				if (ppu::writePort(emu, addr, value)) return;
				break;
			case 3: //[$6000,$8000) SRAM
                emu->mmc.ram.bank6[addr&0x1FFF]=value;
				return;
			case 4: //[$8000,$A000)
			case 5: //[$A000,$C000)
			case 6: //[$C000,$E000)
			case 7: //[$E000,$FFFF]
				//  mapper write
				if (mapper::write(emu, addr, value)) return;
				break;
			case 2: //[$4000,$6000) Other Registers
				switch (valueOf(addr))
				{
				// SPR-RAM DMA Pointer Register
				case 0x4014:
					ERROR_UNLESS(value<0x8, INVALID_MEMORY_ACCESS, MEMORY_CANT_BE_COPIED, "page", value);
					ppu::dma(emu, ramPg(value));
					return;
				// Input Registers 
				case 0x4016:
				case 0x4017:
					if (!(value&1))
					{
						emu->ui.resetInput(emu->ui.ctx);
					}
					return;
				}
				if (addr>=0x4000 && addr<=0x4017)
				{
					// APU Registers
					return;
				}
				break;
		}
		ERROR(INVALID_MEMORY_ACCESS, MEMORY_CANT_BE_WRITTEN, "addr", valueOf(addr), "value", value);
	}
}

namespace mapper
{
	void reset(emu::CTX *emu)
	{
		// MMC1
        emu->mmc.mmc1Sel=INVALID;
        emu->mmc.mmc1Tmp=0;
        emu->mmc.mmc1Pos=0;
		for (int i=0; i<4; i++)
		{
            emu->mmc.mmc1Regs[i].clearAll();
		}

		// MMC3
        emu->mmc.mmc3Control=INVALID;
        emu->mmc.mmc3Cmd=0;
        emu->mmc.mmc3Data=0;
        emu->mmc.mmc3Counter=0;
        emu->mmc.mmc3Latch=INVALID;
        emu->mmc.mmc3IRQ=false;
	}

	void load(emu::CTX *emu, FILE* fp)
	{
		fread(&(emu->mmc.mmc1Sel), sizeof(emu->mmc.mmc1Sel), 1, fp);
		fread(&(emu->mmc.mmc1Pos), sizeof(emu->mmc.mmc1Pos), 1, fp);
		fread(&(emu->mmc.mmc1Tmp), sizeof(emu->mmc.mmc1Tmp), 1, fp);
		fread(&(emu->mmc.mmc1Regs), sizeof(emu->mmc.mmc1Regs), 1, fp);

		fread(&(emu->mmc.mmc3Control), sizeof(emu->mmc.mmc3Control), 1, fp);
		fread(&(emu->mmc.mmc3Cmd), sizeof(emu->mmc.mmc3Cmd), 1, fp);
		fread(&(emu->mmc.mmc3Data), sizeof(emu->mmc.mmc3Data), 1, fp);
		fread(&(emu->mmc.mmc3Counter), sizeof(emu->mmc.mmc3Counter), 1, fp);
		fread(&(emu->mmc.mmc3Latch), sizeof(emu->mmc.mmc3Latch), 1, fp);
		fread(&(emu->mmc.mmc3IRQ), sizeof(emu->mmc.mmc3IRQ), 1, fp);
	}

	void save(emu::CTX *emu, FILE* fp)
	{
		fwrite(&(emu->mmc.mmc1Sel), sizeof(emu->mmc.mmc1Sel), 1, fp);
		fwrite(&(emu->mmc.mmc1Pos), sizeof(emu->mmc.mmc1Pos), 1, fp);
		fwrite(&(emu->mmc.mmc1Tmp), sizeof(emu->mmc.mmc1Tmp), 1, fp);
		fwrite(&(emu->mmc.mmc1Regs), sizeof(emu->mmc.mmc1Regs), 1, fp);

		fwrite(&(emu->mmc.mmc3Control), sizeof(emu->mmc.mmc3Control), 1, fp);
		fwrite(&(emu->mmc.mmc3Cmd), sizeof(emu->mmc.mmc3Cmd), 1, fp);
		fwrite(&(emu->mmc.mmc3Data), sizeof(emu->mmc.mmc3Data), 1, fp);
		fwrite(&(emu->mmc.mmc3Counter), sizeof(emu->mmc.mmc3Counter), 1, fp);
		fwrite(&(emu->mmc.mmc3Latch), sizeof(emu->mmc.mmc3Latch), 1, fp);
		fwrite(&(emu->mmc.mmc3IRQ), sizeof(emu->mmc.mmc3IRQ), 1, fp);
	}

	bool setup(emu::CTX *emu)
	{
		switch (rom::mapperType(emu))
		{
		case 0: // no mapper
		case 2: // Mapper 2: UNROM - PRG/16K
		case 3: // Mapper 3: CNROM - VROM/8K
		case 7: // Mapper 7: AOROM - PRG/32K, Name Table Select
			if (rom::sizeOfImage(emu) >= 0x8000) // 32K of code or more
				mmc::bankSwitch(emu, 0, 1, 2, 3);
			else if (rom::sizeOfImage(emu) == 0x4000) // 16K of code
				mmc::bankSwitch(emu, 0, 1, 0, 1);
			else
			{
invalidROMSize:
				ERROR(INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "image size", rom::sizeOfImage(emu));
				return false;
			}
			return true;		
		case 1: // Mapper 1: MMC1
			if (rom::count16KPRG(emu)>=1 && rom::count16KPRG(emu)<=16) // at least 16K of code, at most 256K of code
			{
				mmc::bankSwitch(emu, 0, 1, rom::count8KPRG(emu)-2, rom::count8KPRG(emu)-1);
				return true;
			}
			goto invalidROMSize;
		case 4: // Mapper 4: MMC3 - PRG/8K, VROM/2K/1K, VT, SRAM, IRQ
			if (rom::count8KPRG(emu)>=2)
			{
				mmc::bankSwitch(emu, 0, 1, rom::count8KPRG(emu)-2, rom::count8KPRG(emu)-1);
				return true;
			}
			goto invalidROMSize;
		}
		// unknown mapper
		FATAL_ERROR(INVALID_ROM, UNSUPPORTED_MAPPER_TYPE, "mapper", rom::mapperType(emu));
		return false;
	}

	byte_t maskPRG(emu::CTX *emu, byte_t bank, const byte_t count)
	{
		assert(count!=0);
		if (bank<count)
			return bank;
		else
		{
			byte_t m=0xFF;
			for (m=0xFF;(bank&m)>=count;m>>=1);
			return bank&m;
		}
	}

	static void selectFirst16KROM(emu::CTX *emu, const byte_t value) // for Mapper 2
	{
		ERROR_IF(((int)value+1)*2>=rom::count8KPRG(emu), INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "bankSize", 16, "num", value);
		mmc::bankSwitch(emu, (value<<1), (value<<1)+1, INVALID, INVALID);
	}

	static void select32KROM(emu::CTX *emu, const byte_t value) // for MMC1
	{
		ERROR_UNLESS((int)value*4<rom::count8KPRG(emu), INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "bankSize", 32, "num", value);
		mmc::bankSwitch(emu, (value<<2), (value<<2)+1, (value<<2)+2, (value<<2)+3);
	}

	bool write(emu::CTX *emu, const maddr_t addr, const byte_t value)
	{
		switch (rom::mapperType(emu))
		{
		case 0: // no mapper
			return false;
		case 1: // Mapper 1:
			return mmc1Write(emu, addr, value);
		case 2: // Mapper 2: Select 16K ROM
			selectFirst16KROM(emu, value);
			return true;
		case 3: // Mapper 3: Select 8K VROM
			pmapper::select8KVROM(emu, value&3);
			return true;
		case 4: // MMC3:
			return mmc3Write(emu, addr, value);
		case 7: // Mapper 7: Select 32K ROM & Name Table Select
			select32KROM(emu, value&7);
			rom::setMirrorMode(emu, (value&16)?MIRRORING::HSINGLESCREEN:MIRRORING::LSINGLESCREEN);
			return true;
		}
		FATAL_ERROR(INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "addr", valueOf(addr), "value", value, "mapper", rom::mapperType(emu));
		return false;
	}

	void HBlank(emu::CTX *emu)
	{
		switch (rom::mapperType(emu))
		{
			case 4: // MMC3:
				mmc3HBlank(emu);
				break;
		}
	}

	// various mapper handlers
	static void mmc1Apply(emu::CTX *emu, const byte_t sel)
	{
		byte_t value=emu->mmc.mmc1Regs[sel](MMC1REG::MASK);
		switch (sel)
		{
		case 0: // Configuration Register
			// configure mirroring
			switch (mmc1Cfg(MMC1REG::NT_MIRRORING))
			{
			case 0:
				rom::setMirrorMode(emu, MIRRORING::LSINGLESCREEN);
				break;
			case 1:
				rom::setMirrorMode(emu, MIRRORING::HSINGLESCREEN);
				break;
			case 2:
				rom::setMirrorMode(emu, MIRRORING::VERTICAL);
				break;
			case 3:
				rom::setMirrorMode(emu, MIRRORING::HORIZONTAL);
				break;
			}
			break;

		case 1: // Select 4K or 8K VROM bank at 0000h
			if (rom::count8KCHR(emu)>0)
			{
				if (mmc1Cfg[MMC1REG::VROM_SWITCH_MODE])
				{
					// 4K
					ERROR_IF((int)value>=rom::count4KCHR(emu), INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "bankSize", 4, "num", value);
					pmapper::selectVROM(emu, 4, value, 0);
				}else
				{
					// 8K
					ERROR_IF((int)value>=rom::count8KCHR(emu), INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "bankSize", 8, "num", value);
					pmapper::select8KVROM(emu, value);
				}
			}
			break;

		case 2: // Select 4K VROM bank at 1000h (4K mode only)
			if (rom::count8KCHR(emu)>0)
			{
				FATAL_ERROR_IF(mmc1Cfg[MMC1REG::VROM_SWITCH_MODE], ILLEGAL_OPERATION, MAPPER_FAILURE, "mmc1reg2", value);
				ERROR_IF((int)value>=rom::count4KCHR(emu), INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "bankSize", 4, "num", value);
				pmapper::selectVROM(emu, 4, value, 1);
			}
			break;

		case 3: // Select 16K or 2x16K ROM bank
			value=emu->mmc.mmc1Regs[sel](MMC1REG::PRG_BANK);
			switch (mmc1Cfg(MMC1REG::PRG_SWITCH_MODE))
			{
			case 0:
			case 1: // Switchable 32K Area at 8000h-FFFFh
				select32KROM(emu, value);
				break;
			case 2: // Switchable 16K Area at C000h-FFFFh
				mmc::bankSwitch(emu, 0, 1, (value<<1), (value<<1)+1);
				break;
			case 3: // Switchable 16K Area at 8000h-BFFFh
				mmc::bankSwitch(emu, (value<<1), (value<<1)+1, rom::count8KPRG(emu)-2, rom::count8KPRG(emu)-1);
				break;
			}
			break;
		}
	}

	bool mmc1Write(emu::CTX *emu, const maddr_t addr, const byte_t value)
	{
		// And I hope the address is multiple of 0x2000.
		vassert(0==(addr&0x1FFF));

		const byte_t i=(valueOf(addr)>>13)&3;
		vassert(i<4);
		assert(emu->mmc.mmc1Sel==INVALID || emu->mmc.mmc1Sel==i);
        emu->mmc.mmc1Sel=i;

		if (value&0x80)
		{
			// clear shift register
            emu->mmc.mmc1Regs[0].asBitField()=valueOf(emu->mmc.mmc1Regs[0])|0xC; // ?
            emu->mmc.mmc1Pos=0;
            emu->mmc.mmc1Tmp=0;
            emu->mmc.mmc1Sel=INVALID;
			return true;
		}else
		{
			// D1-D7 had better be zero.
			//vassert(0==(value&0xFE));

			// serial load (LSB first)
			vassert((emu->mmc.mmc1Pos)<5);
            emu->mmc.mmc1Tmp&=~(1<< (emu->mmc.mmc1Pos));
            emu->mmc.mmc1Tmp|=((value&1)<<(emu->mmc.mmc1Pos));

			// increase position
			(emu->mmc.mmc1Pos)++;
			if ((emu->mmc.mmc1Pos)==5)
			{
				// fifth write
				// copy to selected register
                emu->mmc.mmc1Regs[i].asBitField()= emu->mmc.mmc1Tmp;
				mmc1Apply(emu, emu->mmc.mmc1Sel);
				// reset
                emu->mmc.mmc1Pos=0;
                emu->mmc.mmc1Tmp=0; // optional
                emu->mmc.mmc1Sel=INVALID;
			}
			return true;
		}
		return false;
	}

	static void mmc3Apply(emu::CTX *emu)
	{
		FATAL_ERROR_IF((emu->mmc.mmc3Control)==INVALID, ILLEGAL_OPERATION, MAPPER_FAILURE, "mmc3data", emu->mmc.mmc3Data);
		
		const bool CHRSelect=((emu->mmc.mmc3Control)&0x80)==0x80;
		const bool PRGSelect=((emu->mmc.mmc3Control)&0x40)==0x40;

		switch (emu->mmc.mmc3Cmd) // Command Number
		{
		case 0: // Select 2x1K VROM at PPU 0000h-07FFh
			//assert(0==(mmc3Data&1));
			//pmapper::selectVROM(2, mmc3Data>>1, CHRSelect?2:0); 
			pmapper::selectVROM(emu, 1, emu->mmc.mmc3Data, CHRSelect?4:0);
			pmapper::selectVROM(emu, 1, (emu->mmc.mmc3Data)+1, CHRSelect?5:1);
			break;
		case 1: // Select 2x1K VROM at PPU 0800h-0FFFh
			//assert(0==(mmc3Data&1));
			//pmapper::selectVROM(2, mmc3Data>>1, CHRSelect?3:1); 
			pmapper::selectVROM(emu, 1, emu->mmc.mmc3Data, CHRSelect?6:2);
			pmapper::selectVROM(emu, 1, (emu->mmc.mmc3Data)+1, CHRSelect?7:3);
			break;
		case 2: // Select 1K VROM at PPU 1000h-13FFh
			pmapper::selectVROM(emu, 1, emu->mmc.mmc3Data, CHRSelect?0:4);
			break;
		case 3: // Select 1K VROM at PPU 1400h-17FFh
			pmapper::selectVROM(emu, 1, emu->mmc.mmc3Data, CHRSelect?1:5);
			break;
		case 4: // Select 1K VROM at PPU 1800h-1BFFh
			pmapper::selectVROM(emu, 1, emu->mmc.mmc3Data, CHRSelect?2:6);
			break;
		case 5: // Select 1K VROM at PPU 1C00h-1FFFh
			pmapper::selectVROM(emu, 1, emu->mmc.mmc3Data, CHRSelect?3:7);
			break;
		case 6: // Select 8K ROM at 8000h-9FFFh
			if (!PRGSelect)
				mmc::bankSwitch(emu, emu->mmc.mmc3Data, INVALID, rom::count8KPRG(emu)-2, rom::count8KPRG(emu)-1);
			else
				mmc::bankSwitch(emu, rom::count8KPRG(emu)-2, INVALID, emu->mmc.mmc3Data, rom::count8KPRG(emu)-1);
			break;
		case 7: // Select 8K ROM at A000h-BFFFh
			if (!PRGSelect)
				mmc::bankSwitch(emu, INVALID, emu->mmc.mmc3Data, rom::count8KPRG(emu)-2, rom::count8KPRG(emu)-1);
			else
				mmc::bankSwitch(emu, rom::count8KPRG(emu)-2, emu->mmc.mmc3Data, INVALID, rom::count8KPRG(emu)-1);
			break;
		}
	}

	bool mmc3Write(emu::CTX *emu, const maddr_t addr, const byte_t value)
	{
		switch (valueOf(addr))
		{
		case 0x8000: // Index/Control (5bit)
            emu->mmc.mmc3Cmd=value&7;
            emu->mmc.mmc3Control=value;
			return true;
		case 0x8001: // Data Register
            emu->mmc.mmc3Data=value;
			mmc3Apply(emu);
			return true;
		case 0xA000: // Mirroring Select
			rom::setMirrorMode(emu, (value&1)?MIRRORING::HORIZONTAL:MIRRORING::VERTICAL);
			return true;
		case 0xA001: // SaveRAM Toggle
			mmc::setSRAMEnabled(emu, (value&1)==1);
			return true;
		case 0xC000: // IRQ Counter Register
            emu->mmc.mmc3Counter=value;
			return true;
		case 0xC001: // IRQ Latch Register
            emu->mmc.mmc3Latch=value;
			return true;
		case 0xE000: // IRQ Control Register 0
            emu->mmc.mmc3IRQ=false;
            emu->mmc.mmc3Counter= emu->mmc.mmc3Latch;
			return true;
		case 0xE001: // IRQ Control Register 1
            emu->mmc.mmc3IRQ=true;
			return true;
		}
		return false;
	}

	void mmc3HBlank(emu::CTX *emu)
	{
		if (ppu::currentScanline(emu)==-1)
            emu->mmc.mmc3Counter= emu->mmc.mmc3Latch;
		else if (ppu::currentScanline(emu)>=0 && ppu::currentScanline(emu)<=239)
		{
			if (emu->mmc.mmc3IRQ && render::enabled(emu))
			{
                emu->mmc.mmc3Counter=((emu->mmc.mmc3Counter)-1)&0xFF;
				if (!(emu->mmc.mmc3Counter))
				{
					cpu::irq(emu, IRQTYPE::IRQ);
				}
			}
		}
	}
}
}
