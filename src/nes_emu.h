#pragma once

#include "internals.h"
#include "debug.h"
#include "cpu.h"
#include "mmc.h"
#include "ppu.h"
#include "rom.h"
#include "ui.h"

namespace nes
{

namespace emu
{
    struct CTX {
        debug::CTX debug;
        cpu::CTX cpu;
        mmc::CTX mmc;
        opcode::CTX opcode;
        ppu::CTX ppu;
        rom::CTX rom;
        ui::CTX ui;
    };

	// global functions
	void init(emu::CTX *emu);
	void deinit(emu::CTX *emu);

	bool load(emu::CTX *emu, uint8_t *rom_image, uint32_t rom_size);
	void reset(emu::CTX *emu);
	bool setup(emu::CTX *emu);

	bool nextFrame(emu::CTX *emu);
	bool tick(emu::CTX *emu);
	void run(emu::CTX *emu);

	long long frameCount(emu::CTX *emu);
	
	// proxy functions
	void present(emu::CTX *emu, const int y, const palindex_t line[], const int line_width, const rgb16_t *palette);
	void onFrameBegin(emu::CTX *emu);
	void onFrameEnd(emu::CTX *emu);

	// save state
	void saveState(emu::CTX *emu, FILE *fp);
	void loadState(emu::CTX *emu, FILE *fp);
}
}
