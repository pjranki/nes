#pragma once

#include "types.h"
//#include <assert.h>
#define assert(...) ((void)0)
#include "bitfield.h"

// verbose assertion
#ifdef VERBOSE
#define vassert(e) assert(e)
#else
#define vassert(e) ((void)0)
#endif

#define NES_RGB565_TO_RGB32(a) (((((((unsigned long)a & 0xF800) >>11) * 255) / 31) << 16) | ((((((unsigned long)a & 0x07E0) >>5) * 255) / 63) << 8) | (((((unsigned long)a & 0x001F)) * 255) / 31))

#define NES_RGB32_TO_RGB565(a) ((unsigned short)((((((unsigned long)a & 0x00FF0000) >> 16) >> 3) << 11) + (((((unsigned long)a & 0x0000FF00) >> 8) >> 2) << 5) + (((unsigned long)a & 0x000000FF) >> 3)))

#define NES_R_G_B_TO_RGB565(r,g,b) ((unsigned short)((((((unsigned short)r & 0x000000FF)) >> 3) << 11) + (((((unsigned short)g & 0x000000FF)) >> 2) << 5) + (((unsigned short)b & 0x000000FF) >> 3)))

#define NES_RGB565_AVERAGE_4_VAL(v1, v2, v3, v4) ((((((unsigned long)v1 & 0xF800) + ((unsigned long)v2 & 0xF800) + ((unsigned long)v3 & 0xF800) + ((unsigned long)v4 & 0xF800)) >> 1) & 0xF800) + (((((unsigned long)v1 & 0x07E0) + ((unsigned long)v2 & 0x07E0) + ((unsigned long)v3 & 0x07E0) + ((unsigned long)v4 & 0x07E0)) >> 1) & 0x07E0) + (((((unsigned long)v1 & 0x001F) + ((unsigned long)v2 & 0x001F) + ((unsigned long)v3 & 0x001F) + ((unsigned long)v4 & 0x001F)) >> 1) & 0x001F))

namespace nes
{
    typedef uint16_t _addr16_t;
    typedef uint16_t _addr15_t;
    typedef uint16_t _addr14_t;
    typedef uint8_t  _addr8_t;
    typedef uint8_t  _reg8_t;
    typedef uint16_t _alutemp_t;
    typedef uint8_t  byte_t;
    typedef uint16_t word_t;
    typedef uint32_t uint_t;

    // address
    typedef bit_field<_addr16_t, 16> maddr_t;
    typedef bit_field<_addr15_t, 15> scroll_t, addr15_t;
    typedef bit_field<_addr14_t, 14> vaddr_t, addr14_t;
    typedef bit_field<_addr8_t, 8> maddr8_t, saddr_t;

    // cpu
    typedef byte_t opcode_t;
    typedef byte_t operand_t;

    // alu
    typedef bit_field<byte_t, 8> operandb_t;
    typedef bit_field<word_t, 16> operandw_t;
    typedef bit_field<_alutemp_t, 8> alu_t;

    // ppu
    typedef _reg8_t ioreg_t;
    typedef bit_field<byte_t, 8> tileid_t;
    typedef bit_field<uint8_t, 6> colorindex_t;
    typedef bit_field<uint8_t, 5> palindex_t;

    // color
    typedef uint32_t rgb32_t;
    typedef uint16_t rgb16_t, rgb15_t;

    // others
    typedef bit_field<unsigned, 3> offset3_t;
    typedef bit_field<unsigned, 10> offset10_t;

    // utils
    inline word_t makeWord(byte_t lo, byte_t hi)
    {
        vassert(lo <= 0xFF && hi <= 0xFF);
        return ((word_t)lo) | (((word_t)hi) << 8);
    }

    inline word_t getWord(void *addr)
    {
        return makeWord(*((uint8_t*)addr), *(((uint8_t*)addr) + 1));
    }

    inline void setWord(void *addr, word_t word)
    {
        *((uint8_t*)addr) = (uint8_t)(word & 0xFF);
        *(((uint8_t*)addr)+1) = (uint8_t)((word >> 8) & 0xFF);
    }

    inline rgb32_t Rgb32(const byte_t r, const byte_t g, const byte_t b)
    {
        return b | (g << 8) | (r << 16);
    }

    // hardware configuration
#ifndef LEFT_CLIP
    const int SCREEN_WIDTH = 256;
    const int SCREEN_XOFFSET = 0;
#else
    const int SCREEN_WIDTH = 248;
    const int SCREEN_XOFFSET = 8;
#endif
#ifdef SHOW_240_LINES
    const int SCREEN_HEIGHT = 240;
    const int SCREEN_YOFFSET = 0;
#else
    const int SCREEN_HEIGHT = 224;
    const int SCREEN_YOFFSET = 8;
#endif

    const int SCANLINE_CYCLES = 113;

    // errors
    enum EMUERROR {
        INVALID_ROM = 1,
        INVALID_MEMORY_ACCESS,
        INVALID_INSTRUCTION,
        ILLEGAL_OPERATION
    };

    enum EMUERRORSUBTYPE {
        // INVALID_ROM
        INVALID_FILE_SIGNATURE,
        INVALID_ROM_CONFIG,
        UNEXPECTED_END_OF_FILE,
        UNSUPPORTED_MAPPER_TYPE,

        // INVALID_MEMORY_ACCESS
        MAPPER_FAILURE,
        ADDRESS_OUT_OF_RANGE,
        ILLEGAL_ADDRESS_WARP,
        MEMORY_NOT_EXECUTABLE,
        MEMORY_CANT_BE_READ,
        MEMORY_CANT_BE_WRITTEN,
        MEMORY_CANT_BE_COPIED,

        // INVALID_INSTRUCTION
        INVALID_OPCODE,
        INVALID_ADDRESS_MODE,

        // ILLEGAL_OPERATION
        IRQ_ALREADY_PENDING
    };
}
