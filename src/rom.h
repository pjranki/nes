#pragma once

namespace nes
{
namespace emu
{
    struct CTX;
}

// rom config
enum class MIRRORING {
	MIN=0,
    HORIZONTAL=0,
    VERTICAL,
	FOURSCREEN,
    LSINGLESCREEN,
    HSINGLESCREEN,
	MAX=HSINGLESCREEN
};

enum class ROMCONTROL1 {
    VERTICALM=0x1,
    BATTERYPACK=0x2,
    TRAINER=0x4,
    FOURSCREEN=0x8,
    MAPPERLOW=0xF0
};

enum class ROMCONTROL2 {
    RESERVED=0xF,
    MAPPERHIGH=0xF0
};

// global functions
namespace rom
{
    typedef void (NESAPI *imgcpyFn)(void *ctx, void *dst, void *src, uint32_t size);

    struct CTX {
        void *ctx;
        imgcpyFn imgcpy;
        MIRRORING mirroring;
        uint8_t mapper;
        uint8_t prgCount, chrCount;
        flag_set<uint8_t, ROMCONTROL1> romCtrl;
        flag_set<uint8_t, ROMCONTROL2> romCtrl2;
        char *trainerData;
        size_t trainerSize;
        char *imageData;
        size_t imageSize;
        char *vromData;
        size_t vromSize;
    };

	bool load(emu::CTX *emu, uint8_t *rom_image, uint32_t rom_size);
	void unload(emu::CTX *emu);

	int mapperType(emu::CTX *emu);
	MIRRORING mirrorMode(emu::CTX *emu);
	void setMirrorMode(emu::CTX *emu, MIRRORING newMode);

	const char* getImage(emu::CTX *emu);
	int count16KPRG(emu::CTX *emu);
	int count8KPRG(emu::CTX *emu);
	size_t sizeOfImage(emu::CTX *emu);

	const char* getVROM(emu::CTX *emu);
	int count8KCHR(emu::CTX *emu);
	int count4KCHR(emu::CTX *emu);
	size_t sizeOfVROM(emu::CTX *emu);
}
}
