#pragma once

#include "mmc.h"
#include "nes_emu.h"

namespace nes
{
namespace mmc
{

    extern inline __forceinline opcode_t fetchOpcode(emu::CTX *emu, maddr_t& pc)
    {
        opcode_t opcode;
#ifdef WANT_MEM_PROTECTION
        // check if address in code section [$8000, $FFFF]
        FATAL_ERROR_UNLESS(valueOf(pc) >= 0x6000, INVALID_MEMORY_ACCESS, MEMORY_NOT_EXECUTABLE, "PC", valueOf(pc));
        //opcode = emu->mmc.ram.bank8[pc-0x8000];
        opcode = emu->mmc.ram.data_byte(emu, pc);
#else
        WARN_IF(!MSB(pc), INVALID_MEMORY_ACCESS, MEMORY_NOT_EXECUTABLE, "PC", valueOf(pc));
        opcode = emu->mmc.ram.data_byte(emu, pc);
#endif
        inc(pc);
        return opcode;
    }

    extern inline __forceinline operandb_t fetchByteOperand(emu::CTX *emu, maddr_t& pc)
    {
        operandb_t operand;
#ifdef WANT_MEM_PROTECTION
        //operand(emu->mmc.ram.bank8[pc-0x8000]);
        operand(emu->mmc.ram.data_byte(emu, pc));
#else
        operand(emu->mmc.ram.data_byte(emu, pc));
#endif
        inc(pc);
        return operand;
    }

    extern inline __forceinline operandw_t fetchWordOperand(emu::CTX *emu, maddr_t& pc)
    {
        operandw_t operand;
        FATAL_ERROR_IF(pc.reachMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);
        operand(emu->mmc.ram.data_word(emu, pc));
        pc += 2;
        return operand;
    }

    extern inline __forceinline byte_t loadZPByte(emu::CTX *emu, const maddr8_t zp)
    {
        return ram0p[zp];
    }

    extern inline __forceinline word_t loadZPWord(emu::CTX *emu, const maddr8_t zp)
    {
#ifndef ALLOW_ADDRESS_WRAP
        FATAL_ERROR_IF(zp.reachMax(), INVALID_MEMORY_ACCESS, ILLEGAL_ADDRESS_WARP);
#else
        if (zp.reachMax())
        {
            return (((word_t)ram0p[0]) << 8) | ram0p[zp];
        }
#endif
        return getWord(&(ram0p[zp]));
    }
}
}
