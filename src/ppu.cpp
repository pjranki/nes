#include "macros.h"
#include "types.h"
#include "internals.h"
#include "debug.h"
#include "rom.h"
#include "cpu.h"
#include "ppu.h"
#include "mmc.h"
#include "nes_emu.h"
#include <string.h>

namespace nes
{
namespace mem
{

    static void resetToggle(emu::CTX *emu)
    {
        emu->ppu.firstWrite = true;
        emu->ppu.latch = INVALID;
    }

    static bool toggle(emu::CTX *emu)
    {
        const bool ret = emu->ppu.firstWrite;
        emu->ppu.firstWrite = !emu->ppu.firstWrite;
        return ret;
    }

    static void reset(emu::CTX *emu)
    {
        // reset bank-switching state
        memset(emu->ppu.prevBankSrc, -1, sizeof(emu->ppu.prevBankSrc));

        // clear memory
        memset(&(emu->ppu.vram), 0, sizeof(emu->ppu.vram));
        memset(&(emu->ppu.oam), 0, sizeof(emu->ppu.oam));
    }

    static void copyBanks(emu::CTX *emu, const int dest, const int src, const int count)
    {
        assert((dest + count) * 0x400 <= 0x2000);
        assert((src + count) * 0x400 <= (int)rom::sizeOfVROM(emu));
        memcpy(&vramData(dest * 0x400), rom::getVROM(emu) + src * 0x400, count * 0x400);
    }

    void bankSwitch(emu::CTX *emu, const int dest, const int src, const int count)
    {
        assert(dest >= 0 && dest + count <= 8);
        assert(src != INVALID && src >= 0);
        for (int i = 0; i < count; i++)
        {
            if (emu->ppu.prevBankSrc[dest + i] != src + i)
            {
                emu->ppu.prevBankSrc[dest + i] = src + i;
                copyBanks(emu, dest + i, src + i, 1);
            }
        }
    }

    static bool saveCompleteMemory(emu::CTX *emu)
    {
#ifdef SAVE_COMPLETE_MEMORY
        return true;
#else
        // force complete memory dump when CHR banks don't exist in the rom.
        return (rom::count8KCHR(emu) == 0);
#endif
    }

    static void save(emu::CTX *emu, FILE *fp)
    {
        // memory
        if (saveCompleteMemory(emu))
        {
            fwrite(&(emu->ppu.vram), sizeof(emu->ppu.vram), 1, fp);
        }
        else
        {
            // skip vrom
            fwrite(&(emu->ppu.vram.nameTables), sizeof(emu->ppu.vram.nameTables), 1, fp);
            fwrite(&(emu->ppu.vram.pal), sizeof(emu->ppu.vram.pal), 1, fp);
        }
        fwrite(&(emu->ppu.oam), sizeof(emu->ppu.oam), 1, fp);

        // toggle
        fwrite(&(emu->ppu.firstWrite), sizeof(emu->ppu.firstWrite), 1, fp);

        // latch
        fwrite(&(emu->ppu.latch), sizeof(emu->ppu.latch), 1, fp);

        // bank-switching state
        fwrite(emu->ppu.prevBankSrc, sizeof(emu->ppu.prevBankSrc), 1, fp);
    }

    static void load(emu::CTX *emu, FILE *fp)
    {
        // memory
        if (saveCompleteMemory(emu))
        {
            fread(&(emu->ppu.vram), sizeof(emu->ppu.vram), 1, fp);
        }
        else
        {
            // skip vrom
            fread(&(emu->ppu.vram.nameTables), sizeof(emu->ppu.vram.nameTables), 1, fp);
            fread(&(emu->ppu.vram.pal), sizeof(emu->ppu.vram.pal), 1, fp);
        }
        fread(&(emu->ppu.oam), sizeof(emu->ppu.oam), 1, fp);

        // toggle
        fread(&(emu->ppu.firstWrite), sizeof(emu->ppu.firstWrite), 1, fp);

        // latch
        fread(&(emu->ppu.latch), sizeof(emu->ppu.latch), 1, fp);

        // bank-switching state
        if (saveCompleteMemory(emu))
        {
            fread(emu->ppu.prevBankSrc, sizeof(emu->ppu.prevBankSrc), 1, fp);
        }
        else
        {
            int bankSrc[8];
            STATIC_ASSERT(sizeof(bankSrc) == sizeof(emu->ppu.prevBankSrc));
            fread(bankSrc, sizeof(bankSrc), 1, fp);
            for (int i = 0; i < 8; i++)
            {
                bankSwitch(emu, i, bankSrc[i], 1);
            }
        }
    }

    static vaddr_t ntMirror(emu::CTX *emu, vaddr_flag_t vaddr)
    {
        switch (rom::mirrorMode(emu))
        {
        case MIRRORING::HORIZONTAL:
            vaddr.change<PPUADDR::NT_H>(vaddr[PPUADDR::NT_V]);
        case MIRRORING::VERTICAL:
            vaddr -= PPUADDR::NT_V;
            break;
        case MIRRORING::LSINGLESCREEN:
            vaddr.update<PPUADDR::NT>(0);
            break;
        case MIRRORING::HSINGLESCREEN:
            vaddr.update<PPUADDR::NT>(1);
            break;
        case MIRRORING::FOURSCREEN:
            // no mirroring
            break;
        }
        return vaddr;
    }

    vaddr_t mirror(emu::CTX *emu, vaddr_flag_t vaddr, bool forRead)
    {
        assert(emu->ppu.address.mask(PPUADDR::UNUSED) == 0);
        switch (vaddr.select(PPUADDR::BANK))
        {
        case 0:
        case 1: // [$0,$2000)
            // no mirroring
            break;
        case 2: // [$2000,$3000)
        mirror2000:
            vaddr = ntMirror(emu, vaddr);
            break;
        case 3: // [$3000,$4000)
            if (vaddr.select(PPUADDR::BANK_OFFSET) < 0xF00) {
                vaddr.update<PPUADDR::BANK>(2);
                goto mirror2000;
            }
            // [$3F00,$3FFF]
            vaddr.asBitField() &= 0x3F1F;
            if (vaddr.select(PPUADDR::PAL_ITEM) == 0)
            {
                if (vaddr[PPUADDR::PAL_SELECT])
                {
                    // sprite palette is selected
                    // mirror $3F10, $3F14, $3F18, $3F1C to $3F00, $3F04, $3F08, $3F0C
                    vaddr -= PPUADDR::PAL_SELECT;
                }
                /*else if (forRead)
                {
                    // background palette is selected
                    // $3F04, $3F08, $3F0C can contain unique data,
                    // but PPU uses palette index 0 instead when rendering
                    vaddr-=PPUADDR::PAL_NUM;
                }*/
            }
            break;
        }
        vaddr.clear(PPUADDR::UNUSED);
        return vaddr;
    }

    static void incAddress(emu::CTX *emu)
    {
        assert(emu->ppu.address.mask(PPUADDR::UNUSED) == 0);
        emu->ppu.address.asBitField() += emu->ppu.control[PPUCTRL::VERTICAL_WRITE] ? 32 : 1;
    }

    static void setAddress(emu::CTX *emu, const byte_t byte) // $2006
    {
        if (emu->ppu.firstWrite) // higher 6 bits
        {
            //vassert((byte&~0x3F)==0);
            emu->ppu.tmpAddress.update<PPUADDR::HIGH_BYTE>(byte & 0x3F);

            // store in Reload register temporarily
            address2.copy<PPUADDR::FIRST_WRITE_LO, 0, 2>(byte);
            emu->ppu.control.copy<PPUCTRL::CURRENT_NT, 2, 2>(byte);
            address2.copy<PPUADDR::FIRST_WRITE_HI, 4, 2>(byte);
        }
        else // lower 8 bits
        {
            emu->ppu.tmpAddress.update<PPUADDR::LOW_BYTE>(byte);

            {
                // ?
                address2.update<PPUADDR::LOW_BYTE>(byte);
            }
            address1.update<PPUADDR::LOW_BYTE>(byte);
            address1.update<PPUADDR::FIRST_WRITE_LO>(address2.select(PPUADDR::FIRST_WRITE_LO));
            address1.update<PPUADDR::FIRST_WRITE_MID>(emu->ppu.control.select(PPUCTRL::CURRENT_NT));
            address1.update<PPUADDR::FIRST_WRITE_HI>(address2.select(PPUADDR::FIRST_WRITE_HI));
            address1.asBitField() &= address1.asBitField().MAX;
            // check if correctly set
            assert(valueOf(emu->ppu.tmpAddress) == valueOf(address1));
        }
        emu->ppu.firstWrite = !emu->ppu.firstWrite;
    }

    static byte_t read(emu::CTX *emu)
    {
        const vaddr_t addr = mirror(emu, emu->ppu.address, true);
        incAddress(emu);
        if (addr < 0x3F00)
        {
            // return buffered data
            const byte_t oldLatch = emu->ppu.latch;
            emu->ppu.latch = vramData(addr);
            return oldLatch;
        }
        // no buffering for palette memory access
        return vramData(addr);
    }

    static bool canWrite(emu::CTX *emu)
    {
        return !emu->ppu.status[PPUSTATUS::WRITEIGNORED];
    }

    static void write(emu::CTX *emu, const byte_t data)
    {
        // make sure it's safe to write
        // assert(canWrite());

        const vaddr_t addr = mirror(emu, emu->ppu.address, false);
        {
            // ?
            // resetToggle();
        }
#ifdef WANT_MEM_PROTECTION
        if (rom::count8KCHR(emu) > 0)
        {
            // don't allow writes to vrom if the rom file has any CHR-ROM data.
            ERROR_IF(addr < 0x2000, INVALID_MEMORY_ACCESS, MEMORY_CANT_BE_WRITTEN, "vaddress", valueOf(emu->ppu.address), "actual vaddress", valueOf(addr));
        }
#endif
        vramData(addr) = data;
        incAddress(emu);
    }
}

namespace render
{

    static void setScroll(emu::CTX *emu, const byte_t byte)
    {
        if (mem::toggle(emu))
        {
            // set horizontal scroll
            emu->ppu.xoffset = byte & 7;
            emu->ppu.scroll.update<PPUADDR::XSCROLL>(byte >> 3);
        }
        else
        {
            // set vertical scroll
            emu->ppu.scroll.update<PPUADDR::YOFFSET>(byte & 7);
            emu->ppu.scroll.update<PPUADDR::YSCROLL>(byte >> 3);
        }
    }

    static void getReload(emu::CTX *emu, int *FH, int *HT, int *VT, int *NT, int *FV)
    {
        if (FH) *FH = emu->ppu.xoffset;
        if (HT) *HT = emu->ppu.scroll.select(PPUADDR::TILE_H);
        if (VT) *VT = emu->ppu.scroll.select(PPUADDR::TILE_V);
        if (NT) *NT = emu->ppu.control.select(PPUCTRL::CURRENT_NT);
        if (FV) *FV = emu->ppu.scroll.select(PPUADDR::YOFFSET);
    }

    static void reloadVertical(emu::CTX *emu)
    {
        int VT, NT, FV;
        getReload(emu, 0, 0, &VT, &NT, &FV);
        emu->ppu.address.update<PPUADDR::TILE_V>(VT);
        emu->ppu.address.update<PPUADDR::NT_V>(NT >> 1);
        emu->ppu.address.update<PPUADDR::YOFFSET>(FV);
    }

    static void reloadHorizontal(emu::CTX *emu, int *FH = 0)
    {
        int HT, NT;
        getReload(emu, FH, &HT, 0, &NT, 0);
        emu->ppu.address.update<PPUADDR::TILE_H>(HT);
        emu->ppu.address.update<PPUADDR::NT_H>(NT & 1);
    }

    static void clear(emu::CTX *emu)
    {
        // clear back buffer
        memset(emu->ppu.vBufferLine, 0, sizeof(emu->ppu.vBufferLine));
    }

    static void reset(emu::CTX *emu)
    {
        render::clear(emu);

        emu->ppu.pendingSpritesCount = 0;
        memset(emu->ppu.pendingSprites, -1, sizeof(emu->ppu.pendingSprites));
    }

    bool enabled(emu::CTX *emu)
    {
        return emu->ppu.mask[PPUMASK::BG_VISIBLE] || emu->ppu.mask[PPUMASK::SPR_VISIBLE];
    }

    bool leftClipping(emu::CTX *emu)
    {
#ifdef LEFT_CLIP
        return true;
#else
        return emu->ppu.mask[PPUMASK::BG_CLIP8] || emu->ppu.mask[PPUMASK::SPR_CLIP8];
#endif
    }

    static void present(emu::CTX *emu)
    {
        rgb16_t p32[PALLET_SIZE_SELECTED] = { 0 };
        if (enabled(emu))
        {
            // cache palette colors
            for (int i = 0; i < PALLET_SIZE_SELECTED; i++) p32[i] = emu->ppu.pal32[colorIdx(i)];
        }

        // display
        if (emu->ppu.scanline >= SCREEN_YOFFSET)
        {
            emu::present(emu, emu->ppu.scanline - SCREEN_YOFFSET, &(emu->ppu.vBufferLine[SCREEN_XOFFSET]), SCREEN_WIDTH, p32);
        }
    }

    static void startVBlank(emu::CTX *emu)
    {
        // present frame onto screen
        // set VBlank flag
        emu->ppu.status |= PPUSTATUS::VBLANK;
        // allow writes
        emu->ppu.status -= PPUSTATUS::WRITEIGNORED;
        // do NMI
        if (emu->ppu.control[PPUCTRL::NMI_ENABLED])
        {
            cpu::irq(emu, IRQTYPE::NMI);
        }
    }

    static void duringVBlank(emu::CTX *emu)
    {
        // ? keep VBlank flag turned on
        {
            // status|=PPUSTATUS::VBLANK;
        }
    }

    static void endVBlank(emu::CTX *emu)
    {
        // clear VBlank flag
        emu->ppu.status -= PPUSTATUS::VBLANK;
        // clear HIT flag
        emu->ppu.status -= PPUSTATUS::HIT;
    }

    static void preRender(emu::CTX *emu)
    {
        if (enabled(emu))
        {
            reloadVertical(emu);
            emu->ppu.status |= PPUSTATUS::WRITEIGNORED;
        }
    }

    static void postRender(emu::CTX *emu)
    {
    }

    static void beginFrame(emu::CTX *emu)
    {
        emu::onFrameBegin(emu);
    }

    static void endFrame(emu::CTX *emu)
    {
        ++(emu->ppu.frameNum);

        emu::onFrameEnd(emu);
    }

    static void drawBackground(emu::CTX *emu)
    {
        if (emu->ppu.mask[PPUMASK::BG_VISIBLE])
        {
            // determine origin
            int xoffset;
            reloadHorizontal(emu, &xoffset);
            int startX = (emu->ppu.address(PPUADDR::XSCROLL) << 3) + xoffset;
            const int startY = (emu->ppu.address(PPUADDR::YSCROLL) << 3) + emu->ppu.address(PPUADDR::YOFFSET);

            assert(startX >= 0 && startX < 256);
            assert(startY >= 0 && startY < 256);

            // determine what tables to use for the first part
            vaddr_flag_t mirrored(mem::ntMirror(emu, emu->ppu.address));
            const NESVRAM::NAMEATTRIB_TABLE::NAME_TABLE *nt;
            const NESVRAM::NAMEATTRIB_TABLE::ATTRIBUTE_TABLE *attr;
            const NESVRAM::VROM::PATTERN_TABLE *pt;
            nt = &vramNt(mirrored(PPUADDR::NT));
            attr = &vramAt(mirrored(PPUADDR::NT));
            pt = &vramPt(emu->ppu.control[PPUCTRL::BG_PATTERN] ? 1 : 0);

            // determine tile position in current name table
            const int tileRow = (startY >> 3) % 30;
            const int tileYOffset = startY & 7;

            for (int tileCounter = (startX >> 3); tileCounter <= 31; tileCounter++)
            {
                const tileid_t tileIndex(nt->tiles[tileRow][tileCounter]);
                const int X = (tileCounter << 3) + 7 - startX;

                // look up the tile in attribute table to find its color (D2 and D3)
                const byte_t colorD2D3 = attr->lookup(tileRow, tileCounter);

                // look up the tile in pattern table to find its color (D0 and D1)
                const byte_t colorD0 = pt->tiles[tileIndex].colorD0[tileYOffset];
                const byte_t colorD1 = pt->tiles[tileIndex].colorD1[tileYOffset];

                for (int pixel = min(X, 7); pixel >= 0; pixel--)
                {
                    // Note: B0 indicates the color at the 7th pixel of the tile
                    const byte_t colorD0D1 = ((colorD0 >> pixel) & 1) | (((colorD1 >> pixel) << 1) & 2);
                    const byte_t color = colorD0D1 | colorD2D3;
                    // write to frame buffer
                    vassert(X - pixel >= 0 && X - pixel < RENDER_WIDTH);
                    emu->ppu.vBufferLine[X - pixel] = color;
                }
            }

            if (startX >= 0)
            {
                // now render for the second part
                // switch across to the next tables
                {
                    // ?
                    emu->ppu.address.flip(PPUADDR::NT_H);
                }
                mirrored = mem::ntMirror(emu, emu->ppu.address);
                nt = &vramNt(mirrored(PPUADDR::NT));
                attr = &vramAt(mirrored(PPUADDR::NT));

                const int endTile = (startX + 7) >> 3;
                for (int tileCounter = 0; tileCounter < endTile; tileCounter++)
                {
                    const tileid_t tileIndex(nt->tiles[tileRow][tileCounter]);
                    const int X = (tileCounter << 3) + 7 + (256 - startX);

                    // look up the tile in attribute table to find its color (D2 and D3)
                    const byte_t colorD2D3 = attr->lookup(tileRow, tileCounter);

                    // look up the tile in pattern table to find its color (D0 and D1)
                    const byte_t colorD0 = pt->tiles[tileIndex].colorD0[tileYOffset];
                    const byte_t colorD1 = pt->tiles[tileIndex].colorD1[tileYOffset];

                    for (int pixel = max(X - 255, 0); pixel <= 7; pixel++)
                    {
                        // Note: B0 indicates the color at the 7th pixel of the tile
                        const byte_t colorD0D1 = ((colorD0 >> pixel) & 1) | (((colorD1 >> pixel) << 1) & 2);
                        const byte_t color = colorD0D1 | colorD2D3;
                        // write to frame buffer
                        vassert(X - pixel >= 0 && X - pixel < RENDER_WIDTH);
                        emu->ppu.vBufferLine[X - pixel] = color;
                    }
                }
            }

            // set address to next scanline
            if (emu->ppu.address.inc(PPUADDR::YOFFSET) == 0)
            {
                if (emu->ppu.address.inc(PPUADDR::YSCROLL) == 30)
                {
                    emu->ppu.address.update<PPUADDR::YSCROLL>(0);
                    emu->ppu.address.flip(PPUADDR::NT_V);
                    // no need to update scroll reload
                }
            }
        }
    }

    static void evaluateSprites(emu::CTX *emu)
    {
        emu->ppu.pendingSpritesCount = 0;

        if (emu->ppu.mask[PPUMASK::SPR_VISIBLE])
        {
            emu->ppu.status -= PPUSTATUS::COUNTGT8;

            // find sprites that are within y range for the scanline
            const int sprHeight = emu->ppu.control[PPUCTRL::LARGE_SPRITE] ? 16 : 8;

            for (int i = 0; i < 64; i++)
            {
                if (oamSprite(i).yminus1 < (emu->ppu.scanline) && oamSprite(i).yminus1 + sprHeight >= (emu->ppu.scanline))
                {
#ifdef SPRITE_LIMIT
                    if (emu->ppu.pendingSpritesCount >= 8)
                    {
                        // more than 8 sprites appear in this scanline
                        emu->ppu.status |= PPUSTATUS::COUNTGT8;
                        break;
                    }
                    else
#endif
                    {
                        emu->ppu.pendingSprites[emu->ppu.pendingSpritesCount++] = i;
                    }
                }
            }

#ifdef MONITOR_RENDERING
            // count visible sprites
            emu->ppu.visibleFrontSpriteCount = 0;
            emu->ppu.visibleBackSpriteCount = 0;
            for (int i = 63; i >= 0; i--)
            {
                if (oamSprite(i).yminus1 < 239)
                {
                    if (oamSprite(i).attrib[SPRATTR::BEHIND_BG])
                        emu->ppu.visibleBackSpriteCount++;
                    else
                        emu->ppu.visibleFrontSpriteCount++;
                }
            }
#endif
            for (int i = 0; i < RENDER_WIDTH; i++)
            {
                emu->ppu.solidPixel[i] = ((emu->ppu.vBufferLine[i] & 3) != 0); // indicate whether a background pixel is opaque
                emu->ppu.spritePixel[i] = false;
            }
        }
    }

    static void drawSprites(emu::CTX *emu)
    {
        if (emu->ppu.pendingSpritesCount > 0)
        {
            const int sprWidth = 8;
            const int sprHeight = emu->ppu.control[PPUCTRL::LARGE_SPRITE] ? 16 : 8;

            for (int i = 0; i < (emu->ppu.pendingSpritesCount); i++)
            {
                const int sprId = emu->ppu.pendingSprites[i];
                const auto spr = oamSprite(sprId);
                const bool behindBG = spr.attrib[SPRATTR::BEHIND_BG];

                // get the sprite info
                int sprYOffset = (emu->ppu.scanline) - (spr.yminus1 + 1);
                vassert(sprYOffset >= 0 && sprYOffset < sprHeight);
                if (spr.attrib[SPRATTR::FLIP_V])
                {
                    sprYOffset = sprHeight - 1 - sprYOffset;
                }
                const byte_t colorD2D3 = spr.attrib.select(SPRATTR::COLOR_HI) << 2;

                for (int pixel = 0; pixel < sprWidth; pixel++)
                {
                    const int X = spr.x + pixel;
                    if (X > 255) break;

                    const NESVRAM::VROM::PATTERN_TABLE *pt;
                    const int tileXOffset = spr.attrib[SPRATTR::FLIP_H] ? (sprWidth - 1 - pixel) : pixel;
                    const int tileYOffset = sprYOffset & 7;
                    tileid_t tileIndex;
                    if (emu->ppu.control[PPUCTRL::LARGE_SPRITE])
                    {
                        tileIndex = (spr.tile&~1) | (sprYOffset >> 3);
                        pt = &vramPt(spr.tile & 1);
                    }
                    else
                    {
                        tileIndex = spr.tile;
                        pt = &vramPt(emu->ppu.control[PPUCTRL::SPR_PATTERN] ? 1 : 0);
                    }

                    // look up the tile in pattern table to find its color (D0 and D1)
                    const byte_t colorD0 = pt->tiles[tileIndex].colorD0[tileYOffset];
                    const byte_t colorD1 = pt->tiles[tileIndex].colorD1[tileYOffset];

                    const byte_t colorD0D1 = ((colorD0 >> (7 - tileXOffset)) & 1) | (((colorD1 >> (7 - tileXOffset)) << 1) & 2);
                    const byte_t color = colorD0D1 | colorD2D3 | 0x10;

                    if (colorD0D1) // opaque sprite pixel
                    {
                        // sprite 0 hit detection (regardless priority)
                        if (sprId == 0 && !(emu->ppu.status)[PPUSTATUS::HIT] && (emu->ppu.solidPixel)[X] && (emu->ppu.mask)[PPUMASK::BG_VISIBLE] && !(leftClipping(emu) && X < 8) && X != 255)
                        {
                            // background is non-transparent here
                            emu->ppu.status |= PPUSTATUS::HIT;
                        }
                        // write to frame buffer
                        if (!(emu->ppu.spritePixel[X]))
                        {
                            if (!behindBG || !(emu->ppu.solidPixel[X])) emu->ppu.vBufferLine[X] = color;
                            emu->ppu.spritePixel[X] = true;
                        }
                    }

                    // debug only
                    // if (behindBG) vBuffer[scanline][X]=color;
                }
            }
        }
    }

    static void renderScanline(emu::CTX *emu)
    {
        if (enabled(emu))
        {
            if ((emu->ppu.scanline) >= 0 && (emu->ppu.scanline) <= 239)
            {
#ifdef MONITOR_RENDERING
                debug::info(emu, "[P] --- Scanline %03d --- Sprite 0: (%d, %d) %c%c%c Scroll=[%3d,%3d] %d+%d visible\n", emu->ppu.scanline, oamSprite(0).x, oamSprite(0).yminus1 + 1,
                    (rom::mirrorMode(emu) == MIRRORING::HORIZONTAL) ? 'H' : 'V',
                    emu->ppu.mask[PPUMASK::SPR_VISIBLE] ? 'S' : '-',
                    emu->ppu.mask[PPUMASK::BG_VISIBLE] ? 'B' : '-',
                    emu->ppu.scroll(PPUADDR::XSCROLL) * 8 + emu->ppu.xoffset,
                    emu->ppu.scroll(PPUADDR::YSCROLL) * 8 + emu->ppu.scroll(PPUADDR::YOFFSET),
                    emu->ppu.visibleFrontSpriteCount, emu->ppu.visibleBackSpriteCount);
#endif
                drawBackground(emu);
                evaluateSprites(emu);
                drawSprites(emu);
            }
            else
            {
                // dummy scanline
            }
        }
    }

    static bool HBlank(emu::CTX *emu)
    {
        if (emu->ppu.scanline == -1)
        {
            beginFrame(emu);
            preRender(emu);
        }
        else if (emu->ppu.scanline >= 0 && emu->ppu.scanline <= 239)
        {
            // visible scanlines
            renderScanline(emu);
            // render current line to ui
            present(emu);
        }
        else if (emu->ppu.scanline == 240)
        {
            // dummy scanline
            renderScanline(emu);
            postRender(emu);
            // enter vblank
            startVBlank(emu);
        }
        else if (emu->ppu.scanline >= 241 && emu->ppu.scanline <= 259)
        {
            duringVBlank(emu);
        }
        else if (emu->ppu.scanline == 260)
        {
            endVBlank(emu);
        }
        else if (emu->ppu.scanline == 261)
        {
            endFrame(emu);
            emu->ppu.scanline = -1;
            return false;
        }
        emu->ppu.scanline++;
        return true;
    }

    static void loadNTSCPal(emu::CTX *emu)
    {
        emu->ppu.pal32[0] = NES_R_G_B_TO_RGB565(117, 117, 117);
        emu->ppu.pal32[1] = NES_R_G_B_TO_RGB565(39, 27, 143);
        emu->ppu.pal32[2] = NES_R_G_B_TO_RGB565(0, 0, 171);
        emu->ppu.pal32[3] = NES_R_G_B_TO_RGB565(71, 0, 159);
        emu->ppu.pal32[4] = NES_R_G_B_TO_RGB565(143, 0, 119);
        emu->ppu.pal32[5] = NES_R_G_B_TO_RGB565(171, 0, 19);
        emu->ppu.pal32[6] = NES_R_G_B_TO_RGB565(167, 0, 0);
        emu->ppu.pal32[7] = NES_R_G_B_TO_RGB565(127, 11, 0);
        emu->ppu.pal32[8] = NES_R_G_B_TO_RGB565(67, 47, 0);
        emu->ppu.pal32[9] = NES_R_G_B_TO_RGB565(0, 71, 0);
        emu->ppu.pal32[10] = NES_R_G_B_TO_RGB565(0, 81, 0);
        emu->ppu.pal32[11] = NES_R_G_B_TO_RGB565(0, 63, 23);
        emu->ppu.pal32[12] = NES_R_G_B_TO_RGB565(27, 63, 95);
        emu->ppu.pal32[13] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[14] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[15] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[16] = NES_R_G_B_TO_RGB565(188, 188, 188);
        emu->ppu.pal32[17] = NES_R_G_B_TO_RGB565(0, 115, 239);
        emu->ppu.pal32[18] = NES_R_G_B_TO_RGB565(35, 59, 239);
        emu->ppu.pal32[19] = NES_R_G_B_TO_RGB565(131, 0, 243);
        emu->ppu.pal32[20] = NES_R_G_B_TO_RGB565(191, 0, 191);
        emu->ppu.pal32[21] = NES_R_G_B_TO_RGB565(231, 0, 91);
        emu->ppu.pal32[22] = NES_R_G_B_TO_RGB565(219, 43, 0);
        emu->ppu.pal32[23] = NES_R_G_B_TO_RGB565(203, 79, 15);
        emu->ppu.pal32[24] = NES_R_G_B_TO_RGB565(139, 115, 0);
        emu->ppu.pal32[25] = NES_R_G_B_TO_RGB565(0, 151, 0);
        emu->ppu.pal32[26] = NES_R_G_B_TO_RGB565(0, 171, 0);
        emu->ppu.pal32[27] = NES_R_G_B_TO_RGB565(0, 147, 59);
        emu->ppu.pal32[28] = NES_R_G_B_TO_RGB565(0, 131, 139);
        emu->ppu.pal32[29] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[30] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[31] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[32] = NES_R_G_B_TO_RGB565(255, 255, 255);
        emu->ppu.pal32[33] = NES_R_G_B_TO_RGB565(63, 191, 255);
        emu->ppu.pal32[34] = NES_R_G_B_TO_RGB565(95, 151, 255);
        emu->ppu.pal32[35] = NES_R_G_B_TO_RGB565(167, 139, 253);
        emu->ppu.pal32[36] = NES_R_G_B_TO_RGB565(247, 123, 255);
        emu->ppu.pal32[37] = NES_R_G_B_TO_RGB565(255, 119, 183);
        emu->ppu.pal32[38] = NES_R_G_B_TO_RGB565(255, 119, 99);
        emu->ppu.pal32[39] = NES_R_G_B_TO_RGB565(255, 155, 59);
        emu->ppu.pal32[40] = NES_R_G_B_TO_RGB565(243, 191, 63);
        emu->ppu.pal32[41] = NES_R_G_B_TO_RGB565(131, 211, 19);
        emu->ppu.pal32[42] = NES_R_G_B_TO_RGB565(79, 223, 75);
        emu->ppu.pal32[43] = NES_R_G_B_TO_RGB565(88, 248, 152);
        emu->ppu.pal32[44] = NES_R_G_B_TO_RGB565(0, 235, 219);
        emu->ppu.pal32[45] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[46] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[47] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[48] = NES_R_G_B_TO_RGB565(255, 255, 255);
        emu->ppu.pal32[49] = NES_R_G_B_TO_RGB565(171, 231, 255);
        emu->ppu.pal32[50] = NES_R_G_B_TO_RGB565(199, 215, 255);
        emu->ppu.pal32[51] = NES_R_G_B_TO_RGB565(215, 203, 255);
        emu->ppu.pal32[52] = NES_R_G_B_TO_RGB565(255, 199, 255);
        emu->ppu.pal32[53] = NES_R_G_B_TO_RGB565(255, 199, 219);
        emu->ppu.pal32[54] = NES_R_G_B_TO_RGB565(255, 191, 179);
        emu->ppu.pal32[55] = NES_R_G_B_TO_RGB565(255, 219, 171);
        emu->ppu.pal32[56] = NES_R_G_B_TO_RGB565(255, 231, 163);
        emu->ppu.pal32[57] = NES_R_G_B_TO_RGB565(227, 255, 163);
        emu->ppu.pal32[58] = NES_R_G_B_TO_RGB565(171, 243, 191);
        emu->ppu.pal32[59] = NES_R_G_B_TO_RGB565(179, 255, 207);
        emu->ppu.pal32[60] = NES_R_G_B_TO_RGB565(159, 255, 243);
        emu->ppu.pal32[61] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[62] = NES_R_G_B_TO_RGB565(0, 0, 0);
        emu->ppu.pal32[63] = NES_R_G_B_TO_RGB565(0, 0, 0);
    }
}

namespace ppu
{
    void reset(emu::CTX *emu)
    {
        // reset all registers
        control1.clearAll();
        control2.clearAll();
        emu->ppu.status.clearAll();

        address1.clearAll();
        address2.clearAll();
        emu->ppu.tmpAddress.clearAll();

        // reset counters
        emu->ppu.scanline = -1;
        emu->ppu.frameNum = 0;

        // reset renderer
        render::reset(emu);

        // reset memory
        mem::reset(emu);
    }

    void save(emu::CTX *emu, FILE *fp)
    {
        // registers
        fwrite(&(emu->ppu.control), sizeof(emu->ppu.control), 1, fp);
        fwrite(&(emu->ppu.mask), sizeof(emu->ppu.mask), 1, fp);
        fwrite(&(emu->ppu.status), sizeof(emu->ppu.status), 1, fp);

        fwrite(&(emu->ppu.scroll), sizeof(emu->ppu.scroll), 1, fp);
        fwrite(&(emu->ppu.xoffset), sizeof(emu->ppu.xoffset), 1, fp);
        fwrite(&(emu->ppu.address), sizeof(emu->ppu.address), 1, fp);

        fwrite(&(emu->ppu.oamAddr), sizeof(emu->ppu.oamAddr), 1, fp);

        // memory
        mem::save(emu, fp);
    }

    void load(emu::CTX *emu, FILE *fp)
    {
        // registers
        fread(&(emu->ppu.control), sizeof(emu->ppu.control), 1, fp);
        fread(&(emu->ppu.mask), sizeof(emu->ppu.mask), 1, fp);
        fread(&(emu->ppu.status), sizeof(emu->ppu.status), 1, fp);

        fread(&(emu->ppu.scroll), sizeof(emu->ppu.scroll), 1, fp);
        fread(&(emu->ppu.xoffset), sizeof(emu->ppu.xoffset), 1, fp);
        fread(&(emu->ppu.address), sizeof(emu->ppu.address), 1, fp);

        fread(&(emu->ppu.oamAddr), sizeof(emu->ppu.oamAddr), 1, fp);

        // memory
        mem::load(emu, fp);
    }

    void init(emu::CTX *emu)
    {
        render::loadNTSCPal(emu);
    }

    bool readPort(emu::CTX *emu, const maddr_t maddress, byte_t& data)
    {
        data = INVALID;
        vassert(1 == valueOf(maddress) >> 13); // [$2000,$4000)
        switch (valueOf(maddress) & 7)
        {
        case 2: // $2002 PPU Status Register
            data = valueOf(emu->ppu.status);
            emu->ppu.status.clear(PPUSTATUS::VBLANK);
            mem::resetToggle(emu);
            return true;
        case 0: // $2000 PPU Control Register 1
        case 1: // $2001 PPU Control Register 2
        case 3: // $2003 Sprite RAM address
        case 5: // $2005 Screen Scroll offsets
        case 6: // $2006 VRAM address
            break; // The above are write-only registers.
        case 4: // $2004 Sprite Memory Read
            data = oamData(emu->ppu.oamAddr);
            return true;
        case 7: // $2007 VRAM read
            data = mem::read(emu);
            return true;
        }
        return false;
    }

    bool writePort(emu::CTX *emu, const maddr_t maddress, const byte_t data)
    {
        vassert(1 == valueOf(maddress) >> 13); // [$2000,$4000)
        switch (valueOf(maddress) & 7)
        {
        case 0: // $2000 PPU Control Register 1
            if (!emu->ppu.control[PPUCTRL::NMI_ENABLED] && (data&(byte_t)PPUCTRL::NMI_ENABLED) && emu->ppu.status[PPUSTATUS::VBLANK])
            {
                // nmi should occur when enabled during VBlank
                cpu::irq(emu, IRQTYPE::NMI);
            }
            control1.asBitField() = data;
            return true;
        case 1: // $2001 PPU Control Register 2
            control2.asBitField() = data;
            return true;
        case 3: // $2003 Sprite RAM address
            emu->ppu.oamAddr = data;
            return true;
        case 4: // $2004 Sprite Memory Data
            oamData(emu->ppu.oamAddr) = data;
            inc(emu->ppu.oamAddr);
            return true;
        case 5: // $2005 Screen Scroll offsets
            render::setScroll(emu, data);
            return true;
        case 6: // $2006 VRAM address
            mem::setAddress(emu, data);
            return true;
        case 7: // $2007 VRAM write
            mem::write(emu, data);
            return true;
        case 2: // $2002 PPU Status Register
            break;
        }
        return false;
    }

    bool hsync(emu::CTX *emu)
    {
#ifdef MONITOR_RENDERING
        debug::printPPUState(emu, emu->ppu.frameNum, emu->ppu.scanline, emu->ppu.status[PPUSTATUS::VBLANK], emu->ppu.status[PPUSTATUS::HIT], emu->ppu.mask[PPUMASK::BG_VISIBLE], emu->ppu.mask[PPUMASK::SPR_VISIBLE]);
#endif
        mapper::HBlank(emu);
        return render::HBlank(emu);
    }

    void dma(emu::CTX *emu, const uint8_t* src)
    {
        assert(src != nullptr);
        memcpy(&(emu->ppu.oam), src, sizeof(emu->ppu.oam));
    }

    int currentScanline(emu::CTX *emu)
    {
        return emu->ppu.scanline;
    }

    long long currentFrame(emu::CTX *emu)
    {
        return emu->ppu.frameNum;
    }
}

namespace pmapper
{
    byte_t maskCHR(emu::CTX *emu, byte_t bank, const byte_t count)
    {
        assert(count != 0);
        if (count == 0)
        {
            return 0;
        }
        else if (!SINGLE_BIT(count))
        {
            byte_t m;
            for (m = 1; m < count; m <<= 1);
            bank &= m - 1;
        }
        else
        {
            bank &= count - 1;
        }
        return bank >= count ? count - 1 : bank;
    }

    void selectVROM(emu::CTX *emu, const int CHRSize, const byte_t value, const byte_t bank)
    {
        mem::bankSwitch(emu,
            bank*CHRSize,
            maskCHR(emu, value, rom::count8KCHR(emu)*(8 / CHRSize))*CHRSize,
            CHRSize
        );
    }

    void select8KVROM(emu::CTX *emu, const byte_t value)
    {
        selectVROM(emu, 8, value, 0);
    }

    bool setup(emu::CTX *emu)
    {
        switch (rom::mapperType(emu))
        {
        case 0: // no mapper
        case 1: // Mapper 1: MMC1
        case 2: // Mapper 2: UNROM - PRG/16K
        case 3: // Mapper 3: CNROM - VROM/8K
        case 4: // MMC3 - PRG/8K, VROM/2K/1K, VT, SRAM, IRQ
        case 7: // Mapper 7: AOROM - PRG/32K, Name Table Select
            if (rom::sizeOfVROM(emu) >= 0x2000) // 8K of texture or more
                mem::bankSwitch(emu, 0, 0, 8);
            else if (rom::sizeOfVROM(emu) > 0)
            {
            invalidVROMSize:
                ERROR(INVALID_MEMORY_ACCESS, MAPPER_FAILURE, "vrom size", rom::sizeOfVROM(emu), "mapper", rom::mapperType(emu));
                return false;
            }
            return true;
        }
        // unknown mapper
        FATAL_ERROR(INVALID_ROM, UNSUPPORTED_MAPPER_TYPE, "mapper", rom::mapperType(emu));
        return false;
    }

}
}
