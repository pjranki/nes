#pragma once

#include "internals.h"
//#include <assert.h>
#define assert(...) ((void)0)
#include <stdio.h>

namespace nes
{
namespace emu
{
    struct CTX;
}

enum class MMC1REG
{
    NT_MIRRORING = 0x3,
    PRG_SWITCH_MODE = 0xC,
    VROM_SWITCH_MODE = 0x10,
    PRG_BANK = 0xF,

    MASK = 0x1F
};

namespace mmc
{
    //__declspec(align(0x1000))
    struct RAM
    {
	    union
	    {
		    // $0000 Internal RAM
		    uint8_t bank0[0x800];

		    struct
		    {
			    // $0100 Zero-Page Memory
			    uint8_t zeropage[0x100];

			    // $0100 Stack
			    uint8_t stack[0x100];
		    };
	    };

    private:
	    // memory space reserved for mirroring/mapping
        //uint8_t __bank0_mirror[0x1800];
        //uint8_t __io[0x3000];
        //uint8_t __ext[0x1000];

    public:
	    // $6000 SaveRAM
	    uint8_t bank6[8192];

        // save space, remove what is not used
	    union
	    {
		    //uint8_t code[0x8000];

		    struct
		    {
			    // $8000 PRG-ROM (8K per bank, 4 built-in banks)
			    //uint8_t _bank8[8192], _bankA[8192];

			    // $C000 Can be mirror of PRG-ROM at $8000
			    //uint8_t _bankC[8192], _bankE[8192];
		    };
	    };
        uint8_t *bank8;
        uint8_t *bankA;
        uint8_t *bankC;
        uint8_t *bankE;


    public:
        uint8_t data_byte(emu::CTX *emu, const size_t ptr);
        uint16_t data_word(emu::CTX *emu, const size_t ptr);
        uint8_t* page(const size_t num);
    };

    struct RAMCACHE {
        uint8_t *bank8_cache;
        uint16_t bank8_cache_size;
        uint16_t bank8_view_count;
        uint8_t *bankA_cache;
        uint16_t bankA_cache_size;
        uint16_t bankA_view_count;
        uint8_t *bankC_cache;
        uint16_t bankC_cache_size;
        uint16_t bankC_view_count;
        uint8_t *bankE_cache;
        uint16_t bankE_cache_size;
        uint16_t bankE_view_count;
    };

    struct CTX {
        RAM ram;
        RAMCACHE cache;

        // addresses of currently selected prg-rom banks.
        int p8, pA, pC, pE;

        bool sramEnabled;

        // MMC1 registers
        ioreg_t mmc1Sel; // register select
        ioreg_t mmc1Pos;
        ioreg_t mmc1Tmp;
        flag_set<ioreg_t, MMC1REG, 5> mmc1Regs[4];
#define mmc1Cfg (emu->mmc.mmc1Regs)[0]

        // MMC3 registers
        ioreg_t mmc3Control;
        ioreg_t mmc3Cmd;
        ioreg_t mmc3Data;
        ioreg_t mmc3Counter;
        ioreg_t mmc3Latch;
        bool mmc3IRQ;
    };

#define ramSt (emu->mmc.ram).stack
#define ram0p (emu->mmc.ram).zeropage
#define ramPg(num) (emu->mmc.ram).page(num)
#define ramData(offset) (emu->mmc.ram).data(offset)

	// global functions
	void reset(emu::CTX *emu);

	void bankSwitch(emu::CTX *emu, int r8, int rA, int rC, int rE);

	byte_t read(emu::CTX *emu, const maddr_t addr);
	void write(emu::CTX *emu, const maddr_t addr, const byte_t value);

	// save state
	void save(emu::CTX *emu, FILE *fp);
	void load(emu::CTX *emu, FILE *fp);
}

namespace mapper
{
	// global functions
	void reset(emu::CTX *emu);
	bool setup(emu::CTX *emu);

	bool write(emu::CTX *emu, const maddr_t addr, const byte_t value);

	void HBlank(emu::CTX *emu);

	bool mmc1Write(emu::CTX *emu, const maddr_t addr, const byte_t value);
	bool mmc3Write(emu::CTX *emu, const maddr_t addr, const byte_t value);

	void mmc3HBlank(emu::CTX *emu);

	byte_t maskPRG(emu::CTX *emu, byte_t bank, const byte_t count);

	// save state
	void save(emu::CTX *emu, FILE *fp);
	void load(emu::CTX *emu, FILE *fp);
}

}
