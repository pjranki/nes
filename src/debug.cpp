#include "macros.h"
#include "types.h"
#include "internals.h"
#include "debug.h"
#include "opcodes.h"
#include "mmc.h"
#include "nes_emu.h"
#include <stdarg.h>
#include <stdlib.h> 
#include <stdio.h>

namespace nes
{
namespace debug
{
    void info(emu::CTX *emu, const char *format, ...)
    {
        char message[NES_MAX_PRINTF_MESSAGE + 1] = { 0 };
        va_list args;
        if (emu->debug.print != nullptr)
        {
            va_start(args, format);
#ifdef _WIN32
            vsprintf_s(message, NES_MAX_PRINTF_MESSAGE, format, args);
#else
            vsnprintf(message, NES_MAX_PRINTF_MESSAGE, format, args);
#endif
            va_end(args);
            emu->debug.print(emu->debug.ctx, message);
        }
    }

	void printDisassembly(emu::CTX *emu, const maddr_t pc, const opcode_t opcode, const _reg8_t rx, const _reg8_t ry, const maddr_t addr, const operand_t operand)
	{
		const opcode::M6502_OPCODE op = opcode::decode(emu, opcode);
		switch (op.size)
		{
		case 1:
			info(emu, "%04X  %02X        %s", valueOf(pc), opcode, opcode::instName(emu, op.inst));
			break;
		case 2:
            info(emu, "%04X  %02X %02X     %s", valueOf(pc), opcode, emu->mmc.ram.data_byte(emu, pc+1), opcode::instName(emu, op.inst));
			break;
		case 3:
            info(emu, "%04X  %02X %02X %02X  %s", valueOf(pc), opcode, emu->mmc.ram.data_byte(emu, pc+1), emu->mmc.ram.data_byte(emu, pc+2), opcode::instName(emu, op.inst));
			break;
		}
		switch (op.addrmode)
		{
		case opcode::ADR_IMP:
			break;
		case opcode::ADR_ZP:
		case opcode::ADR_ZPX:
		case opcode::ADR_ZPY:
            info(emu, " $%02X = %02X", valueOf(addr), operand);
			break;
		case opcode::ADR_REL:
            info(emu, " to $%04X", valueOf(addr));
			break;
		case opcode::ADR_ABS:
		case opcode::ADR_ABSX:
		case opcode::ADR_ABSY:
		case opcode::ADR_INDX:
		case opcode::ADR_INDY:
		case opcode::ADR_IND:
            info(emu, " $%04X = %02X", valueOf(addr), operand);
			break;
		case opcode::ADR_IMM:
            info(emu, " #$%02X", operand);
			break;
		}
        info(emu, "\n");
	}

	void printCPUState(emu::CTX *emu, const maddr_t pc, const _reg8_t ra, const _reg8_t rx, const _reg8_t ry, const _reg8_t rp, const _reg8_t rsp, const int cyc)
	{
        info(emu, "[A:%02X X:%02X Y:%02X P:%02X SP:%02X CYC:%3d] -> %04X\n", ra, rx, ry, rp, rsp, cyc, valueOf(pc));
	}

	void printPPUState(emu::CTX *emu, const long long frameNum, const int scanline, const bool vblank, const bool hit, const bool bgmsk, const bool sprmsk)
	{
        info(emu, "----- FR: %I64d SL: %3d VB:%s HIT:%s MSK:%c%c -----\n", frameNum, scanline, vblank?"True":"false", hit?"Yes":"no",
			bgmsk?'B':'_', sprmsk?'S':'_');
	}

	// a NULL at the end of argv is REQUIRED!
	static void printToConsole(emu::CTX *emu, int type, const char * typestr, int stype, const char * stypestr, const wchar_t * file, const char * function_name, unsigned long line_number, va_list argv)
	{
        info(emu, "Type: %s (%d)\nSub Type: %s (%d)\nProc: %s:%ld\n", typestr, type, stypestr, stype, function_name, line_number);
		if (file != nullptr)
		{
            info(emu, "File: %S\n", file);
		}

		// print custom parameters
		char* name=nullptr;
		while ((name=va_arg(argv, char*))!=nullptr)
		{
			int value=va_arg(argv, int);
            info(emu, "<%s> = %Xh (%d)\n", name, value, value);
		}
	}

	static const char * errorTypeToString(EMUERROR type)
	{
		switch (type)
		{
			CASE_ENUM_RETURN_STRING(INVALID_ROM);
			CASE_ENUM_RETURN_STRING(INVALID_MEMORY_ACCESS);
			CASE_ENUM_RETURN_STRING(INVALID_INSTRUCTION);
			CASE_ENUM_RETURN_STRING(ILLEGAL_OPERATION);

		default: return "UNKNOWN";
		}
	}

	static const char * errorSTypeToString(EMUERRORSUBTYPE stype)
	{
		switch (stype)
		{
			CASE_ENUM_RETURN_STRING(INVALID_FILE_SIGNATURE);
			CASE_ENUM_RETURN_STRING(INVALID_ROM_CONFIG);
			CASE_ENUM_RETURN_STRING(UNEXPECTED_END_OF_FILE);
			CASE_ENUM_RETURN_STRING(UNSUPPORTED_MAPPER_TYPE);

			CASE_ENUM_RETURN_STRING(MAPPER_FAILURE);
			CASE_ENUM_RETURN_STRING(ADDRESS_OUT_OF_RANGE);
			CASE_ENUM_RETURN_STRING(ILLEGAL_ADDRESS_WARP);
			CASE_ENUM_RETURN_STRING(MEMORY_NOT_EXECUTABLE);
			CASE_ENUM_RETURN_STRING(MEMORY_CANT_BE_READ);
			CASE_ENUM_RETURN_STRING(MEMORY_CANT_BE_WRITTEN);
			CASE_ENUM_RETURN_STRING(MEMORY_CANT_BE_COPIED);

			CASE_ENUM_RETURN_STRING(INVALID_OPCODE);
			CASE_ENUM_RETURN_STRING(INVALID_ADDRESS_MODE);

			CASE_ENUM_RETURN_STRING(IRQ_ALREADY_PENDING);

		default: return "UNKNOWN";
		}
	}

	void fatalError(emu::CTX *emu, EMUERROR type, EMUERRORSUBTYPE stype, const wchar_t * file, const char * function_name, unsigned long line_number, ...)
	{
		va_list args;
		va_start(args, line_number);
        info(emu, "[X] Fatal error: \n");
		printToConsole(emu, type, errorTypeToString(type), stype, errorSTypeToString(stype), file, function_name, line_number, args);
		va_end(args);
#ifndef NDEBUG
		assert(0);
#endif
		exit(type);
	}

	void error(emu::CTX *emu, EMUERROR type, EMUERRORSUBTYPE stype, const wchar_t * file, const char * function_name, unsigned long line_number, ...)
	{
		va_list args;
		va_start(args, line_number);
        info(emu, "[X] Error: \n");
		printToConsole(emu, type, errorTypeToString(type), stype, errorSTypeToString(stype), file, function_name, line_number, args);
		va_end(args);
#ifndef NDEBUG
		assert(0);
#else
		__debugbreak();
#endif
	}

	void warn(emu::CTX *emu, EMUERROR type, EMUERRORSUBTYPE stype, const char * function_name, unsigned long line_number, ...)
	{
		va_list args;
		va_start(args, line_number);
        info(emu, "[!] Warning: \n");
		printToConsole(emu, type, errorTypeToString(type), stype, errorSTypeToString(stype), NULL, function_name, line_number, args);
		va_end(args);
	}

}
}
