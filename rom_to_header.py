from argparse import ArgumentParser
import sys
import os
import struct


def rom_to_header(src, dest):
    if not dest.endswith('.h'):
        raise Exception("since your output file is not a header file, I'm going to save you by not doing that")
    with open(src, 'rb') as handle:
        data = handle.read()
    col = 0
    text = '#define BUFFER_SIZE %d\nconst char BUFFER[BUFFER_SIZE] PROGMEM = {\n    ' % (len(data))
    for data_byte in data:
        text += "'\\x%02x'," % (data_byte)
        col += 1
        if col == 16:
            col = 0
            text += '\n    '
    text += '};\n'
    print(text)
    with open(dest, 'wt') as handle:
        handle.write(text)


def main(args):
    parser = ArgumentParser(description="ROM to header")
    parser.add_argument("-i", dest="infile", required=True, type=str)
    parser.add_argument("-o", dest="outfile", required=True, type=str)
    args = parser.parse_args()
    print(args.infile)
    print(args.outfile)
    rom_to_header(args.infile, args.outfile)

if __name__ == '__main__':
    main(sys.argv)
