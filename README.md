NES emulator
============
Port of the NES emulator written by neveraway for Ardunio (tested with ESP8266).

Original source code: https://github.com/neveraway/nes-emulator

Language
--------
C++11 and up.

Arduino
-------
Checkout/copy this library to your Arduino libraries folder.

How to use it
-------------
Include the library:

```cpp
#include <nes.h>
```

Construct the class:

```cpp
nes::Emulator nes_game;
```

You can get the screen size for the game using the following constants:

```cpp
nes::SCREEN_WIDTH;
nes::SCREEN_HEIGHT;
nes::PALLET_SIZE_SELECTED;  // pixels (rgb565/uint16_t) in a palette
```

There is also a helper for averaging pixels in a 2x2 window of pixels into a single pixel:

```cpp
uint16_t top_left = 0;
uint16_t top_right = 0;
uint16_t bottom_left = 0;
uint16_t bottom_right = 0;
uint16_t colour = NES_RGB565_AVERAGE_4_VAL(top_left, top_right, bottom_left, bottom_right);
```

Tell the NES emulator how to do things:

```cpp
nes_game.set_render_line([](uint32_t y, const nes::palindex_t *line, uint32_t line_size, const nes::rgb16_t *palette) -> void
{
    // Draw a line a (0, y)
    // for(uint16_t x = 0; x < line_size; x++) {
    //      each RGB565 (16-bit) pixel can be accessed with palette[line[x]]
    // }
});
nes_game.set_frame_start([]() {
    // Do something on frame start
});
nes_game.set_frame_end([]() {
    // Do something on frame end
    // Arduino: wdt_reset();
});
nes_game.set_log([](const char *message){
    // Log messages from the emulator
    // Arduino: Serial.prinln(message);
});
nes_game.set_memcpy([](void *dst, const void *src, uint32_t size) {
    // Copy game buffer provided by new_game.load(buffer, buffer_size);
    // Arduino: If you game is in PROGMEM, this is where you call memcpy_P(dst, src, size);
    // NOTE: that is memcpy_P (not a typo)
});
```

To create C header files of your games, use the python script:

```bash
py -3 rom_to_header.py -i game.nes -o game.h
```

To load a game:

```cpp
uint32_t game_size = 12345;
const char game[12345] = {...};
nes_game.reset();
nes_game.load(game, game_size);
```

Call the tick function 60 times per second (or as fast as you can):

```cpp
nes_game.tick();
```
